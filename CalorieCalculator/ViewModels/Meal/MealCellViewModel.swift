//
//  MealCellViewModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

final class MealCellViewModel {

  let deleteSubject = PublishSubject<MealModel>()
  let updateSubject = PublishSubject<MealModel>()
  let meal: MealModel

  init(with meal: MealModel) {
    self.meal = meal
  }

}
