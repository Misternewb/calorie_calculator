//
//  MealViewModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import Gnomon
import Astrolabe

final class MealViewModel {

  typealias DayMeals = [Date: [MealCellViewModel]]

  let filter = Variable<MealFilterModel?>(nil)

  private let user: UserModel
  let mealProvider: MealProvider

  init(with user: UserModel, authenticationProvider: AuthenticationProvider) {
    self.user = user
    mealProvider = MealManager(with: authenticationProvider)
  }

  func loadData(for intent: LoaderIntent) -> Observable<DayMeals> {
    let meals = mealProvider.getMeal(for: user, cached: intent == .initial).map { $0.models }
    return meals.withLatestFrom(filter.asObservable()) { meals, filter -> DayMeals in
      let filteredMeals = MealViewModel.filter(meals: meals, by: filter)
      let days: [Date] = filteredMeals.map({ $0.date })
        .reduce([]) { result, date in
          if result.contains(where: { $0.equalsByDay(to: date) }) {
            return result
          }
          return result + [date]
        }
      return days.reduce([:]) { result, day in
        var result = result
        let viewModels = filteredMeals
          .filter { $0.date.equalsByDay(to: day) }
          .sorted { $0.date > $1.date }
          .map { MealCellViewModel(with: $0)}
        result[day] = viewModels
        return result
      }
    }
  }

  private static func filter(meals: [MealModel], by filter: MealFilterModel?) -> [MealModel] {
    guard let filter = filter else {
      return meals
    }

    return meals.filter { filter.apply(to: $0.date) }
  }

}

private extension Date {

  func equalsByDay(to date: Date) -> Bool {
    return Calendar.current.compare(self, to: date, toGranularity: .day) == .orderedSame
  }

}
