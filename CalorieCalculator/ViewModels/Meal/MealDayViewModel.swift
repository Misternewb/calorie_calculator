//
//  MealDayViewModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/13/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

final class MealDayViewModel {

  private let user: Observable<UserModel>
  private let date: Date
  private let meals: [MealModel]

  init(with user: Observable<UserModel>, date: Date, meals: [MealModel]) {
    self.user = user
    self.date = date
    self.meals = meals
  }

  var calories: Calory {
    return meals.reduce(0) { $0 + $1.calories }
  }

  var dateString: String {
    return Formatters.monthDayFormatter.string(from: date)
  }

  var caloriesBelowThreshold: Observable<Bool> {
    return user.map { [calories] in calories <= $0.caloriesThreshold }
  }

}
