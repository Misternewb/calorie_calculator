//
//  AuthenticationViewModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import RxCocoa

final class AuthenticationViewModel: ReactiveCompatible {

  static let passwordRange = 5...30

  enum Mode {
    case login, register
  }

  let authenticationProvider: AuthenticationProvider
  init(with authenticationProvider: AuthenticationProvider) {
    self.authenticationProvider = authenticationProvider
  }

  let mode = Variable(Mode.login)

  func switchMode() {
    switch mode.value {
    case .login: mode.value = .register
    case .register: mode.value = .login
    }
  }

  func validate(email: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let predicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    return predicate.evaluate(with: email)
  }

  enum PasswordValidity {
    case valid, short, long
  }

  func validate(password: String) -> PasswordValidity {
    let count = password.count
    let range = AuthenticationViewModel.passwordRange
    if count < range.lowerBound {
      return .short
    } else if count > range.upperBound {
      return .long
    } else {
      return .valid
    }
  }

  func validate(password: String, repeatPassword: String) -> Bool {
    return password == repeatPassword
  }

  func login(with username: String, password: String) throws -> Observable<LoginModel> {
    return try authenticationProvider.login(with: username, password: password)
  }

  func register(with username: String, password: String) throws -> Observable<RegisterModel> {
    return try authenticationProvider.register(with: username, password: password)
  }

  func loadCurrentUser() -> Observable<APISingleModel<UserModel>> {
    do {
      return try authenticationProvider.loadCurrentUser()
    } catch {
      return .error(APIError.restricted)
    }
  }

}

extension Reactive where Base: AuthenticationViewModel {

  var switchMode: Binder<Void> {
    return Binder(self.base) { viewModel, _ in
      viewModel.switchMode()
    }
  }

}
