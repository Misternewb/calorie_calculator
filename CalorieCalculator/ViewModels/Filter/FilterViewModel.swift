//
//  FilterViewModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/13/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

final class FilterViewModel {

  let fromDateViewModel = FilterCellViewModel(with: .fromDate)
  let toDateViewModel = FilterCellViewModel(with: .toDate)
  let fromTimeViewModel = FilterCellViewModel(with: .fromTime)
  let toTimeViewModel = FilterCellViewModel(with: .toTime)

  init(with filter: MealFilterModel?) {
    guard let filter = filter else {
      return
    }
    fromDateViewModel.selected.value = filter.fromDate != nil
    fromDateViewModel.date.value = filter.fromDate ?? Date()

    toDateViewModel.selected.value = filter.toDate != nil
    toDateViewModel.date.value = filter.toDate ?? Date()

    fromTimeViewModel.selected.value = filter.fromTime != nil
    fromTimeViewModel.date.value = filter.fromTime ?? Date()

    toTimeViewModel.selected.value = filter.toTime != nil
    toTimeViewModel.date.value = filter.toTime ?? Date()
  }

  func buildFilter() -> MealFilterModel? {
    var fromDate = fromDateViewModel.selectedDate
    var toDate = toDateViewModel.selectedDate
    var fromTime = fromTimeViewModel.selectedDate
    var toTime = toTimeViewModel.selectedDate

    if fromDate == nil && toDate == nil && fromTime == nil && toTime == nil {
      return nil
    }

    switch (fromDate, toDate) {
    case let(from, to) as (Date, Date):
      fromDate = min(from, to)
      toDate = max(from, to)
    default:
      break
    }
    switch (fromTime, toTime) {
    case let(from, to) as (Date, Date):
      fromTime = min(from, to)
      toTime = max(from, to)
    default:
      break
    }

    return MealFilterModel(fromDate: fromDate, toDate: toDate,
                           fromTime: fromTime, toTime: toTime)
  }

}
