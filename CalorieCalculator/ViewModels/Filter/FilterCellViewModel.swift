//
//  FilterCellViewModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/13/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

final class FilterCellViewModel {

  enum FilterType {
    case fromDate, toDate, fromTime, toTime

    var id: String {
      switch self {
      case .fromDate: return "from_date"
      case .toDate: return "to_date"
      case .fromTime: return "from_time"
      case .toTime: return "to_time"
      }
    }

    var title: String {
      switch self {
      case .fromDate: return L10n.Filter.fromDate
      case .toDate: return L10n.Filter.toDate
      case .fromTime: return L10n.Filter.fromTime
      case .toTime: return L10n.Filter.toTime
      }
    }

  }

  let type: FilterType

  init(with type: FilterType) {
    self.type = type
  }

  let selected = Variable(false)
  let date = Variable(Date())

  var selectedDate: Date? {
    return selected.value ? date.value : nil
  }

}
