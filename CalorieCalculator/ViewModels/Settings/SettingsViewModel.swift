//
//  SettingsViewModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/10/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

final class SettingsViewModel {

  private let providers = AppDelegate.shared.providers

  var dailyCalories: Observable<Calory> {
    return providers.userUpdate.user
      .asObservable().skipNil().map { $0.caloriesThreshold }
  }

  func update(dailyCalories: Calory) {
    guard var user = providers.userUpdate.user.value else {
      return
    }
    user.caloriesThreshold = dailyCalories
    providers.userUpdate.update(with: user)
  }

  func logout() {
    AppDelegate.shared.providers.authentication.logout()
  }

}
