//
//  UserCellViewModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

final class UserCellViewModel {

  let deleteSubject = PublishSubject<UserModel>()
  let updateSubject = PublishSubject<UserModel>()
  let user: UserModel
  let updateEnabled: Bool
  let deleteEnabled: Bool

  init(with user: UserModel, currentUser: UserModel) {
    self.user = user
    updateEnabled = currentUser.canUpdate(user: user)
    deleteEnabled = currentUser.canDelete(user: user)
  }

}
