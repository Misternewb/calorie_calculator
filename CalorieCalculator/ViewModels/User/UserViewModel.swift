//
//  UserViewModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import Astrolabe

final class UserViewModel {

  private let disposeBag = DisposeBag()

  private let user: UserModel
  let userProvider: UserProvider

  init(user: UserModel, authenticationProvider: AuthenticationProvider) {
    self.user = user
    userProvider = UserManager(with: authenticationProvider)
  }

  func loadData(for intent: LoaderIntent) -> Observable<[UserModel.Role: [UserCellViewModel]]> {
    return userProvider.getUsers(cached: intent == .initial)
      .map { [user] in $0.models.filter { $0.id != user.id } }
      .map { [user] users -> [UserModel.Role: [UserCellViewModel]] in
        let viewModels = users.map { UserCellViewModel(with: $0, currentUser: user) }
        return Dictionary(grouping: viewModels, by: { $0.user.role })
      }
  }

}
