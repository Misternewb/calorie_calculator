//
//  SignedIntegerExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/9/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Foundation

extension SignedInteger {

  static func random<T: SignedInteger>(_ range: ClosedRange<T>) -> T {
    let length = Int64(range.upperBound - range.lowerBound + 1)
    let value = Int64(arc4random()) % length + Int64(range.lowerBound)
    return T(value)
  }

}
