//
//  UIViewExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

extension UIView {

  func addSubviews(_ views: UIView...) {
    addSubviews(views)
  }

  func addSubviews(_ views: [UIView]) {
    for view in views {
      addSubview(view)
    }
  }

  func dropShadow() {
    layer.masksToBounds = false
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.3
    layer.shadowOffset = CGSize(width: 1, height: 1)
    layer.shadowRadius = 2
    layer.shadowPath = UIBezierPath(rect: bounds).cgPath
  }

}
