//
//  UIStackViewExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

extension UIStackView {

  func addArrangedSubviews(_ subviews: [UIView]) {
    subviews.forEach { self.addArrangedSubview($0) }
  }

  func removeArrangedSubviews() {
    arrangedSubviews.forEach { self.removeArrangedSubview($0) }
  }

}
