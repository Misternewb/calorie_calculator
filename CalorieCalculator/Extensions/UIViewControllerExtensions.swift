//
//  UIViewControllerExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import Dodo

extension UIViewController {

  func inNavigation() -> NavigationController {
    return NavigationController(rootViewController: self)
  }

  func showError(with message: String) {
    let dodo = view.dodo
    dodo.style.label.font = Fonts.Lato.semibold.font(size: 15)
    dodo.style.label.numberOfLines = 3
    dodo.style.bar.backgroundColor = Colors.flatRed
    dodo.style.bar.hideAfterDelaySeconds = 3
    dodo.style.bar.hideOnTap = true

    dodo.style.bar.animationShow = DodoAnimations.slideVertically.show
    dodo.style.bar.animationHide = DodoAnimations.slideVertically.hide

    let margin = view.safeAreaLayoutGuide.layoutFrame.minY + 5
    dodo.style.bar.marginToSuperview = CGSize(width: 20, height: margin)
    dodo.show(message)
  }

}
