//
//  UIViewRxExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/13/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: UIView {

  var backgroundColor: Binder<UIColor> {
    return Binder(self.base) { view, color in
      view.backgroundColor = color
    }
  }

}
