//
//  UserDefaultsExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/9/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Foundation

extension UserDefaults {

  func setCodable<T: Codable>(_ value: T?, forKey key: String) {
    let encoder = JSONEncoder()
    let encoded = try? encoder.encode(value)
    set(encoded, forKey: key)
  }

  func codableValue<T: Codable>(forKey key: String) -> T? {
    let decoder = JSONDecoder()
    guard let data = data(forKey: key) else {
      return nil
    }
    return try? decoder.decode(T.self, from: data)
  }

}
