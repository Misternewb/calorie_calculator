//
//  Geometry.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

extension CGFloat {

  static var onePixel: CGFloat {
    return 1 / UIScreen.main.scale
  }

}

extension CGFloat {

  func dividedIntegrally(on numberOfParts: Int) -> [CGFloat] {
    var parts = [CGFloat]()
    var remainingLength = self
    for index in 0 ..< numberOfParts {
      var part = remainingLength / CGFloat(numberOfParts - index)
      if index != numberOfParts - 1 {
        part.round()
      }
      remainingLength -= part
      parts.append(part)
    }
    return parts
  }

}

extension CGAffineTransform {

  var translation: CGPoint {
    return CGPoint(x: tx, y: ty)
  }

}

func + (left: CGAffineTransform, right: CGAffineTransform) -> CGAffineTransform {
  return left.concatenating(right)
}

prefix func ! (transform: CGAffineTransform) -> CGAffineTransform {
  return transform.inverted()
}

func << (left: CGRect, right: CGAffineTransform) -> CGRect {
  return left.applying(right)
}

// MARK: - CGPoint

extension CGPoint {

  func integral() -> CGPoint {
    return CGPoint(x: x.rounded(.towardZero), y: y.rounded(.towardZero))
  }

  func diff(_ other: CGPoint) -> CGPoint {
    return CGPoint(x: x - other.x, y: y - other.y)
  }

  func invert() -> CGPoint {
    return self * -1.0
  }

  func multiply(_ multiplier: CGFloat) -> CGPoint {
    return CGPoint(x: x * multiplier, y: y * multiplier)
  }

  func distance(to other: CGPoint) -> CGFloat {
    return sqrt(pow(x.distance(to: other.x), 2) + pow(y.distance(to: other.y), 2))
  }

}

func << (left: CGPoint, right: CGAffineTransform) -> CGPoint {
  return left.applying(right)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
  return left.diff(right)
}

prefix func - (point: CGPoint) -> CGPoint {
  return point.invert()
}

func * (left: CGPoint, right: CGFloat) -> CGPoint {
  return left.multiply(right)
}

func / (left: CGPoint, right: CGFloat) -> CGPoint {
  return left * (1.0 / right)
}

func * (left: CGPoint, right: CGPoint) -> CGPoint {
  return CGPoint(x: left.x * right.x, y: left.y * right.y)
}

func / (left: CGPoint, right: CGPoint) -> CGPoint {
  return CGPoint(x: left.x / right.x, y: left.y / right.y)
}

func + (left: CGPoint, right: CGPoint) -> CGPoint {
  return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

// MARK: - UIEdgeInsets

extension UIEdgeInsets {

  public static func top(_ size: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: size, left: 0, bottom: 0, right: 0)
  }

  public static func left(_ size: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: size, bottom: 0, right: 0)
  }

  public static func bottom(_ size: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 0, bottom: size, right: 0)
  }

  public static func right(_ size: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: size)
  }

  public static func vertical(_ size: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: size, left: 0, bottom: size, right: 0)
  }

  public static func horizontal(_ size: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: size, bottom: 0, right: size)
  }

  public static func all(_ size: CGFloat) -> UIEdgeInsets {
    return UIEdgeInsets(top: size, left: size, bottom: size, right: size)
  }

  public func top(_ size: CGFloat) -> UIEdgeInsets {
    var other = self
    other.top = size
    return other
  }

  public func left(_ size: CGFloat) -> UIEdgeInsets {
    var other = self
    other.left = size
    return other
  }

  public func bottom(_ size: CGFloat) -> UIEdgeInsets {
    var other = self
    other.bottom = size
    return other
  }

  public func right(_ size: CGFloat) -> UIEdgeInsets {
    var other = self
    other.right = size
    return other
  }

  public func vertical(_ size: CGFloat) -> UIEdgeInsets {
    var other = self
    other.top = size
    other.bottom = size
    return other
  }

  public func horizontal(_ size: CGFloat) -> UIEdgeInsets {
    var other = self
    other.left = size
    other.right = size
    return other
  }

  var inverted: UIEdgeInsets {
    return UIEdgeInsets(top: -top, left: -left, bottom: -bottom, right: -right)
  }

  func inset(rect: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(rect, self)
  }

  func inset(size: CGSize) -> CGSize {
    let rect = CGRect(origin: .zero, size: size)
    return UIEdgeInsetsInsetRect(rect, self).size
  }

}

prefix func - (insets: UIEdgeInsets) -> UIEdgeInsets {
  return insets * CGFloat(-1.0)
}

func * (left: UIEdgeInsets, right: CGFloat) -> UIEdgeInsets {
  return UIEdgeInsets(top: left.top * right, left: left.left * right,
                      bottom: left.bottom * right, right: left.right * right)
}

func / (left: UIEdgeInsets, right: CGFloat) -> UIEdgeInsets {
  return left * (1.0 / right)
}

func + (left: UIEdgeInsets, right: UIEdgeInsets) -> UIEdgeInsets {
  return UIEdgeInsets(
    top: left.top + right.top,
    left: left.left + right.left,
    bottom: left.bottom + right.bottom,
    right: left.right + right.right
  )
}

func += (left: inout UIEdgeInsets, right: UIEdgeInsets) {
  // swiftlint:disable:next shorthand_operator
  left = left + right
}

func - (left: UIEdgeInsets, right: UIEdgeInsets) -> UIEdgeInsets {
  return left + (-right)
}

func -= (left: inout UIEdgeInsets, right: UIEdgeInsets) {
  // swiftlint:disable:next shorthand_operator
  left = left - right
}

// MARK: - CGSize

extension CGSize {

  var integral: CGSize {
    return CGSize(width: width.rounded(.awayFromZero), height: height.rounded(.awayFromZero))
  }

  func scaleAspectRatio(toFit bound: CGSize) -> CGSize {
    let ratio = width / height
    let fitRatio = bound.width / bound.height

    var size = self

    if ratio >= fitRatio {
      let resizeRatio = bound.width / width
      size.width = bound.width
      size.height = height * resizeRatio
    } else {
      let resizeRatio = bound.height / height
      size.width = width * resizeRatio
      size.height = bound.height
    }

    return size
  }

}

func * (left: CGSize, right: CGFloat) -> CGSize {
  return CGSize(width: left.width * right, height: left.height * right)
}

func * (left: CGFloat, right: CGSize) -> CGSize {
  return right * left
}

extension FloatingPoint {

  func scale(min minValue: Self, max maxValue: Self) -> Self {
    let result = self * (maxValue - minValue) + minValue
    let min: (_ x: Self, _ y: Self) -> Self = minValue < maxValue ? Self.minimum: Self.maximum
    let max: (_ x: Self, _ y: Self) -> Self = minValue < maxValue ? Self.maximum: Self.minimum
    return max(min(result, maxValue), minValue)
  }

  // swiftlint:disable:next identifier_name
  func scale(min minValue: Self, max maxValue: Self, c: Self) -> Self {
    let result = (self - c) / (1 - c) * (maxValue - minValue) + minValue
    let min: (_ x: Self, _ y: Self) -> Self = minValue < maxValue ? Self.minimum: Self.maximum
    let max: (_ x: Self, _ y: Self) -> Self = minValue < maxValue ? Self.maximum: Self.minimum
    return max(min(result, maxValue), minValue)
  }

}

extension CGRect {

  var center: CGPoint { return CGPoint(x: midX, y: midY) }
  var topLeft: CGPoint { return CGPoint(x: minX, y: minY) }
  var topRight: CGPoint { return CGPoint(x: maxX, y: minY) }
  var bottomLeft: CGPoint { return CGPoint(x: minX, y: maxY) }
  var bottomRight: CGPoint { return CGPoint(x: maxX, y: maxY) }

}
