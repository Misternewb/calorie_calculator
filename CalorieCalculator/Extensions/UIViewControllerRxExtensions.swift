//
//  UIViewControllerRxExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: UIViewController {

  func present(animated: Bool = true) -> AnyObserver<UIViewController> {
    return AnyObserver { [weak base = self.base] event in
      MainScheduler.ensureExecutingOnScheduler()
      switch event {
      case .next(let viewController):
        base?.present(viewController, animated: animated, completion: nil)
      default: break
      }
    }
  }

  func present(_ viewController: UIViewController, animated: Bool = true) -> Observable<Void> {
    return Observable.create { [weak base = self.base] subscriber -> Disposable in
      base?.present(viewController, animated: animated) {
        subscriber.onNext(())
        subscriber.onCompleted()
      }
      return Disposables.create()
      }.subscribeOn(MainScheduler.instance)
  }

  func dismiss(animated: Bool = true) -> AnyObserver<Void> {
    return AnyObserver { [weak base = self.base] event in
      MainScheduler.ensureExecutingOnScheduler()
      switch event {
      case .next:
        base?.dismiss(animated: animated, completion: nil)
      default: break
      }
    }
  }

  func dismiss(animated: Bool = true) -> Observable<Void> {
    return Observable.create { [weak base = self.base] subscriber -> Disposable in
      base?.dismiss(animated: animated) {
        subscriber.onNext(())
        subscriber.onCompleted()
      }
      return Disposables.create()
    }
  }

}

extension Reactive where Base: UINavigationController {

  func push(animated: Bool = true) -> AnyObserver<UIViewController> {
    return AnyObserver { [weak base = self.base] event in
      MainScheduler.ensureExecutingOnScheduler()
      switch event {
      case .next(let viewController):
        base?.pushViewController(viewController, animated: animated)
      default: break
      }
    }
  }

  func push(_ viewController: UIViewController, animated: Bool = true) -> Observable<Void> {
    return Observable.create { [weak base = self.base] subscriber -> Disposable in
      base?.pushViewController(viewController, animated: animated)
      subscriber.onNext(())
      subscriber.onCompleted()
      return Disposables.create()
    }
  }

  func pop(animated: Bool = true) -> AnyObserver<Void> {
    return AnyObserver { [weak base = self.base] event in
      MainScheduler.ensureExecutingOnScheduler()
      switch event {
      case .next:
        _ = base?.popViewController(animated: animated)
      default: break
      }
    }
  }

  func popViewController(animated: Bool = true) -> Observable<UIViewController?> {
    return Observable.create { [weak base = self.base] subscriber -> Disposable in
      let viewController = base?.popViewController(animated: animated)
      subscriber.onNext(viewController)
      subscriber.onCompleted()
      return Disposables.create()
    }
  }

  func popTo(animated: Bool = true) -> AnyObserver<UIViewController> {
    return AnyObserver { [weak base = self.base] event in
      MainScheduler.ensureExecutingOnScheduler()
      switch event {
      case .next(let viewController):
        _ = base?.popToViewController(viewController, animated: animated)
      default: break
      }
    }
  }

  func pop(to viewController: UIViewController, animated: Bool = true) -> Observable<[UIViewController]?> {
    return Observable.create { [weak base = self.base] subscriber -> Disposable in
      let controllers = base?.popToViewController(viewController, animated: animated)
      subscriber.onNext(controllers)
      subscriber.onCompleted()
      return Disposables.create()
    }
  }

  func popToRootViewController(animated: Bool = true) -> AnyObserver<Void> {
    return AnyObserver { [weak base = self.base] event in
      MainScheduler.ensureExecutingOnScheduler()
      switch event {
      case .next:
        _ = base?.popToRootViewController(animated: animated)
      default: break
      }
    }
  }

  func popToRootViewController(animated: Bool = true) -> Observable<[UIViewController]?> {
    return Observable.create { [weak base = self.base] subscriber -> Disposable in
      let controllers = base?.popToRootViewController(animated: animated)
      subscriber.onNext(controllers)
      subscriber.onCompleted()
      return Disposables.create()
    }
  }

  func set(viewControllers: [UIViewController], animated: Bool = true) -> Observable<Void> {
    return Observable.create { [weak base = self.base] subscriber -> Disposable in
      base?.setViewControllers(viewControllers, animated: animated)
      subscriber.onNext(())
      subscriber.onCompleted()
      return Disposables.create()
    }
  }

  func setViewControllers(animated: Bool = true) -> AnyObserver<[UIViewController]> {
    return AnyObserver { [weak base = self.base] event in
      MainScheduler.ensureExecutingOnScheduler()
      switch event {
      case .next(let viewControllers):
        base?.setViewControllers(viewControllers, animated: animated)
      default: break
      }
    }
  }

}
