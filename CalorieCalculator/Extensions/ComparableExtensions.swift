//
//  ComparableExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

extension Comparable {

  func clamp(_ minValue: Self, _ maxValue: Self) -> Self {
    return min(max(self, minValue), maxValue)
  }

}
