//
//  DateExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/14/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Foundation

extension Calendar {

  func compareTime(date: Date, with anotherDate: Date) -> ComparisonResult {

    var leftDate = Date(timeIntervalSinceReferenceDate: 0)
    var rightDate = Date(timeIntervalSinceReferenceDate: 0)

    let leftComponents = dateComponents([.hour, .minute, .second], from: date)
    let rightComponents = dateComponents([.hour, .minute, .second], from: anotherDate)

    leftDate = self.date(byAdding: leftComponents, to: leftDate) ?? leftDate
    rightDate = self.date(byAdding: rightComponents, to: rightDate) ?? rightDate
    return leftDate.compare(rightDate)
  }

}
