//
//  RxExtensions.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import RxCocoa

infix operator <->: DefaultPrecedence

func <-><T: Equatable>(left: Variable<T>, right: Variable<T>) -> Disposable {
  return bind(left, right)
}

func bind<T: Equatable>(_ left: Variable<T>, _ right: Variable<T>) -> Disposable {
  return bind([left, right])
}

func bind<T: Equatable>(_ first: Variable<T>, _ second: Variable<T>, _ third: Variable<T>) -> Disposable {
  return bind([first, second, third])
}

func bind<T: Equatable>(_ variables: [Variable<T>]) -> Disposable {
  guard variables.count > 1 else { fatalError() }
  let id = "RxCocoa." + UUID().uuidString
  let scheduler = SerialDispatchQueueScheduler(internalSerialQueueName: id)
  let first = variables[variables.startIndex]
  let others = variables[variables.index(after: variables.startIndex) ..< variables.endIndex]
  let firstToOthers = first.asObservable().distinctUntilChanged().observeOn(scheduler).subscribe {
    switch $0 {
    case .next(let value):
      for variable in others where variable.value != value {
        variable.value = value
      }
    default: break
    }
  }
  return Disposables.create(others.map { variable in
    variable.asObservable().skip(1).distinctUntilChanged().observeOn(scheduler).subscribe {
      switch $0 {
      case .next(let value):
        if first.value != value {
          first.value = value
        }
      default: break
      }
    }
    } + [firstToOthers])
}

func bind<T: Equatable>(_ subjects: [BehaviorSubject<T>]) -> Disposable {
  guard subjects.count > 1 else { fatalError() }
  let id = "RxCocoa." + UUID().uuidString
  let scheduler = SerialDispatchQueueScheduler(internalSerialQueueName: id)
  let first = subjects[subjects.startIndex]
  let others = subjects[subjects.index(after: subjects.startIndex) ..< subjects.endIndex]
  let firstToOthers = first.asObservable().distinctUntilChanged().observeOn(scheduler).subscribe(onNext: {
    for subject in others {
      do {
        if try subject.value() != $0 {
          subject.onNext($0)
        }
      } catch {}
    }
  })

  return Disposables.create(others.map {
    $0.asObservable().distinctUntilChanged().observeOn(scheduler).subscribe(onNext: {
      do {
        if try first.value() != $0 {
          first.onNext($0)
        }
      } catch {}
    })
    } + [firstToOthers])
}

func <-><T: Equatable>(left: ControlProperty<T>, right: Variable<T>) -> Disposable {
  let id = "RxCocoa." + UUID().uuidString
  let scheduler = SerialDispatchQueueScheduler(internalSerialQueueName: id)
  let leftToRight = left.asObservable().distinctUntilChanged().observeOn(scheduler).subscribe {
    switch $0 {
    case .next(let value):
      if right.value != value {
        right.value = value
      }
    default: break
    }
  }
  let rightToLeft = right.asObservable().skip(1).distinctUntilChanged().observeOn(scheduler).subscribe {
    switch $0 {
    case .next(let value):
      left.onNext(value)
    default: break
    }
  }

  return Disposables.create(rightToLeft, leftToRight)
}

func <-><T: Equatable>(left: Variable<T>, right: ControlProperty<T>) -> Disposable {
  return right <-> left
}

extension ObservableType {

  func skipNil<T>() -> Observable<T> where E == T? {
    return flatMap { value -> Observable<T> in
      switch value {
      case let .some(value): return Observable<T>.just(value)
      case .none: return Observable<T>.empty()
      }
    }
  }

}
