//
//  APIModels.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Gnomon

enum APIError: Error, DecodableModel {

  case restricted, undefined

  init(from decoder: Decoder) throws {
    self = .undefined
  }

}

extension APIError: LocalizedError {

  var errorDescription: String? {
    switch self {
    case .restricted: return L10n.Api.Errors.restricted
    case .undefined: return L10n.Api.Errors.undefined
    }
  }

}

struct APISingleModel<T: DecodableModel>: DecodableModel {

  let model: T

  private enum CodingKeys: String, CodingKey {
    case error
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let error = try container.decodeIfPresent(APIError.self, forKey: .error) {
      throw error
    } else {
      model = try T(from: decoder)
    }
  }

}

struct APIContainerModel<T: APIDecodable>: DecodableModel {

  let models: [T]

  private enum CodingKeys: String, CodingKey {
    case error
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let error = try container.decodeIfPresent(APIError.self, forKey: .error) {
      throw error
    } else {
      let keyedContainer = try decoder.container(keyedBy: DynamicKey.self)
      models = try keyedContainer.allKeys.map { key -> T in
        try T.init(with: key.stringValue, decoder: try keyedContainer.superDecoder(forKey: key))
      }
    }
  }

}

struct APIResultModel: DecodableModel {

  private enum CodingKeys: String, CodingKey {
    case error
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let error = try container.decodeIfPresent(APIError.self, forKey: .error) {
      throw error
    }
  }

  init() {}

}

private struct DynamicKey: CodingKey {

  let stringValue: String
  init?(stringValue: String) {
    self.stringValue = stringValue
    intValue = Int(stringValue)
  }

  let intValue: Int?
  init?(intValue: Int) {
    self.intValue = intValue
    stringValue = String(intValue)
  }

}
