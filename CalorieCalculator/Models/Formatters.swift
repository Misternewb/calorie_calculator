//
//  Formatters.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Foundation

enum Formatters {

  static let decimalFormatter = NumberFormatter.new {
    $0.numberStyle = .decimal
    $0.roundingMode = .halfUp
    $0.maximumFractionDigits = 1
  }

  static let dateFormatter = DateFormatter.new {
    $0.setLocalizedDateFormatFromTemplate("ddMMyyyy")
  }

  static let dateTimeFormatter = DateFormatter.new {
    $0.setLocalizedDateFormatFromTemplate("ddMMyyyyhhmm")
  }

  static let timeFormatter = DateFormatter.new {
    $0.setLocalizedDateFormatFromTemplate("hhmm")
  }

  static let fullDateFormatter = DateFormatter.new {
    $0.setLocalizedDateFormatFromTemplate("MMMMddEEEE")
  }

  static let monthDayFormatter = DateFormatter.new {
    $0.setLocalizedDateFormatFromTemplate("MMMMdd")
  }

}
