//
//  APIDecodable.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Foundation

protocol APIDecodable {

  var id: String? { get }

  init(with id: String?, decoder: Decoder) throws

}
