//
//  AuthenticationModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Gnomon

struct AuthenticationModel<E: Error & DecodableModel>: DecodableModel {

  let auth: AuthModel

  private enum CodingKeys: String, CodingKey {
    case error
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let error = try container.decodeIfPresent(E.self, forKey: .error) {
      throw error
    } else {
      auth = try AuthModel(from: decoder)
    }
  }

}
