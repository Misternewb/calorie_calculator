//
//  TokenModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Gnomon

struct TokenModel: DecodableModel {

  let authToken: String
  let expiresIn: Int

  private enum CodingKeys: String, CodingKey {
    case authToken = "id_token", expiresIn = "expires_in"
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    authToken = try container.decode(String.self, forKey: .authToken)
    expiresIn = Int(try container.decode(String.self, forKey: .expiresIn)) ?? 0
  }

}
