//
//  RegisterModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Gnomon

enum RegisterError: Error, DecodableModel {

  case emailExists, registrationDisabled, blocked, undefined

  private enum CodingKeys: String, CodingKey {
    case errorCode = "message"
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    let errorCode = try container.decode(String.self, forKey: .errorCode)
    switch errorCode {
    case "EMAIL_EXISTS": self = .emailExists
    case "OPERATION_NOT_ALLOWED": self = .registrationDisabled
    case "TOO_MANY_ATTEMPTS_TRY_LATER": self = .blocked
    default: self = .undefined
    }
  }

}

extension RegisterError: LocalizedError {

  var errorDescription: String? {
    switch self {
    case .emailExists: return L10n.Authentication.RegisterErrors.invalidEmail
    case .registrationDisabled: return L10n.Authentication.RegisterErrors.registrationDisabled
    case .blocked: return L10n.Authentication.RegisterErrors.userBlocked
    case .undefined: return L10n.Authentication.RegisterErrors.undefined
    }
  }

}

typealias RegisterModel = AuthenticationModel<RegisterError>
