//
//  LoginModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Gnomon

enum LoginError: Error, DecodableModel {

  case emailNotFound, invalidPassword, userDisabled, undefined

  private enum CodingKeys: String, CodingKey {
    case errorCode = "message"
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    let errorCode = try container.decode(String.self, forKey: .errorCode)
    switch errorCode {
    case "EMAIL_NOT_FOUND": self = .emailNotFound
    case "INVALID_PASSWORD": self = .invalidPassword
    case "USER_DISABLED": self = .userDisabled
    default: self = .undefined
    }
  }

}

extension LoginError: LocalizedError {

  var errorDescription: String? {
    switch self {
    case .emailNotFound: return L10n.Authentication.LoginErrors.invalidEmail
    case .invalidPassword: return L10n.Authentication.LoginErrors.invalidPassword
    case .userDisabled: return L10n.Authentication.LoginErrors.userDisabled
    case .undefined: return L10n.Authentication.LoginErrors.undefined
    }
  }

}

typealias LoginModel = AuthenticationModel<LoginError>
