//
//  AuthModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/4/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Gnomon

struct AuthModel: DecodableModel, Encodable, Equatable {

  let id: String
  let email: String
  let refreshToken: String

  private enum CodingKeys: String, CodingKey {
    case id = "localId", email, refreshToken
  }

  static func == (lhs: AuthModel, rhs: AuthModel) -> Bool {
    return lhs.id == rhs.id
  }

}
