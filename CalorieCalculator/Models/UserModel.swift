//
//  UserModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Gnomon

struct UserModel: APIDecodable, DecodableModel, Encodable, Equatable {

  enum Role: String, Codable {
    case user, manager, admin

    static let allValues: [Role] = [.user, .manager, .admin]
  }

  let id: String?
  let role: Role
  var caloriesThreshold: Calory
  let email: String
  let password: String?

  private enum CodingKeys: String, CodingKey {
    case id, role, caloriesThreshold = "calories_threshold", email, password
  }

  static func == (lhs: UserModel, rhs: UserModel) -> Bool {
    return lhs.id == rhs.id
      && lhs.role == rhs.role
      && lhs.caloriesThreshold == rhs.caloriesThreshold
      && lhs.email == rhs.email
  }

  init(with id: String?, decoder: Decoder) throws {
    self.id = id
    let container = try decoder.container(keyedBy: CodingKeys.self)
    role = try container.decode(Role.self, forKey: .role)
    caloriesThreshold = try container.decode(Calory.self, forKey: .caloriesThreshold)
    email = try container.decode(String.self, forKey: .email)
    password = nil
  }

  init(with id: String?, role: Role, caloriesThreshold: Calory,
       email: String, password: String? = nil) {
    self.id = id
    self.role = role
    self.caloriesThreshold = caloriesThreshold
    self.email = email
    self.password = password
  }

  var dictionaryValue: [String: Any] {
    return [
      CodingKeys.role.rawValue: role.rawValue,
      CodingKeys.caloriesThreshold.rawValue: caloriesThreshold,
      CodingKeys.email.rawValue: email
    ]
  }

}

extension UserModel {

  var canModifyOthersMeals: Bool {
    return role == .admin
  }

  var canHaveMeals: Bool {
    return role == .user
  }

  var canFetchUsers: Bool {
    return role == .admin || role == .manager
  }

  var canCreateUsers: Bool {
    return role == .admin || role == .manager
  }

  func canUpdate(user: UserModel) -> Bool {
    if user.id == id {
      return true
    }
    switch role {
    case .user:
      return false
    case .admin:
      return true
    case .manager:
      return user.role == .user
    }
  }

  func canDelete(user: UserModel) -> Bool {
    return canUpdate(user: user)
  }

}

extension UserModel.Role {

  var title: String {
    switch self {
    case .user: return L10n.User.Role.user
    case .manager: return L10n.User.Role.manager
    case .admin: return L10n.User.Role.admin
    }
  }

  var color: UIColor {
    switch self {
    case .user: return Colors.roleUser
    case .manager: return Colors.roleManager
    case .admin: return Colors.roleAdmin
    }
  }

}
