//
//  MealModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Gnomon

typealias Calory = Int

struct MealModel: APIDecodable {

  let id: String?
  let name: String
  let date: Date
  let calories: Calory

  private enum CodingKeys: String, CodingKey {
    case name, timestamp, calories
  }

  init(with id: String?, decoder: Decoder) throws {
    self.id = id
    let container = try decoder.container(keyedBy: CodingKeys.self)
    name = try container.decode(String.self, forKey: .name)
    let timestamp = try container.decode(Double.self, forKey: .timestamp)
    date = Date(timeIntervalSince1970: timestamp)
    calories = try container.decode(Calory.self, forKey: .calories)
  }

  init(with id: String?, name: String, date: Date, calories: Calory) {
    self.id = id
    self.name = name
    self.date = date
    self.calories = calories
  }

  var dictionaryValue: [String: Any] {
    return [
      CodingKeys.name.rawValue: name,
      CodingKeys.timestamp.rawValue: Int(date.timeIntervalSince1970),
      CodingKeys.calories.rawValue: calories
    ]
  }

}
