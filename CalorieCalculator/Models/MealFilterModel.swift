//
//  MealFilterModel.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/13/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

struct MealFilterModel: Equatable {

  var fromDate: Date?
  var toDate: Date?
  var fromTime: Date?
  var toTime: Date?

  func apply(to date: Date) -> Bool {
    let calendar = Calendar.current

    if let fromDate = fromDate, calendar.compare(fromDate, to: date, toGranularity: .day) == .orderedDescending {
      return false
    }

    if let toDate = toDate, calendar.compare(date, to: toDate, toGranularity: .day) == .orderedDescending {
      return false
    }

    if let fromTime = fromTime, calendar.compareTime(date: fromTime, with: date) == .orderedDescending {
      return false
    }

    if let toTime = toTime, calendar.compareTime(date: date, with: toTime) == .orderedDescending {
      return false
    }

    return true
  }

  static func == (lhs: MealFilterModel, rhs: MealFilterModel) -> Bool {
    return lhs.fromDate == rhs.fromDate &&
      lhs.toDate == rhs.toDate &&
      lhs.fromTime == rhs.fromTime &&
      lhs.toTime == rhs.toTime
  }

}
