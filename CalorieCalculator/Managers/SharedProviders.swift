//
//  SharedProviders.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/9/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

final class SharedProviders {

  private let disposeBag = DisposeBag()

  let storage: StorageProvider = StorageManager()
  let authentication: AuthenticationProvider
  let userUpdate: UserUpdateProvider

  init() {
    authentication = AuthenticationManager(with: storage)
    let userProvider = UserManager(with: authentication)
    userUpdate = UserUpdateManager(with: nil, userProvider: userProvider)
    userUpdate.user.asObservable().skipNil().distinctUntilChanged { $0 == $1 }
      .observeOn(MainScheduler.asyncInstance)
      .bind(to: authentication.user).disposed(by: disposeBag)
    authentication.user.asObservable()
      .distinctUntilChanged { $0?.id == $1?.id }
      .observeOn(MainScheduler.asyncInstance)
      .bind(to: userUpdate.user).disposed(by: disposeBag)
  }

}
