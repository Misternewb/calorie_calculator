//
//  AuthenticationProvider.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/4/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import Gnomon

protocol AuthenticationProvider: class {

  var tokenProvider: TokenProvider? { get set }
  var auth: Variable<AuthModel?> { get }
  var user: Variable<UserModel?> { get }

  func login(with username: String, password: String) throws -> Observable<LoginModel>
  func register(with username: String, password: String) throws -> Observable<RegisterModel>
  func loadCurrentUser() throws -> Observable<APISingleModel<UserModel>>
  func logout()

}
