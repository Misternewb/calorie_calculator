//
//  TokenManager.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import Gnomon

private enum Constants {

  static let tokenExpirationDelta = 30
  static let tokenRetryDelta = 10

}

final class TokenManager: TokenProvider {

  var token: Observable<TokenModel?> {
    guard let token = tokenVariable.value else {
      return (try? refresh()) ?? .just(nil)
    }
    return .just(token)
  }

  private let tokenVariable = Variable<TokenModel?>(nil)

  private let refreshToken: String

  private let disposeBag = DisposeBag()

  init(refreshToken: String) {
    self.refreshToken = refreshToken
    startRefreshing()
  }

  func refresh() throws -> Observable<TokenModel?> {
    let request = try tokenRequest()
    return Gnomon.models(for: request)
      .map { $0.result.model }
  }

  func invalidate() {
    tokenVariable.value = nil
  }

  private func startRefreshing() {
    do {
      try refresh().catchErrorJustReturn(nil).bind(to: tokenVariable).disposed(by: disposeBag)
    } catch {}
    tokenVariable.asObservable().skip(1)
      .flatMap { [weak self] token -> Observable<TokenModel?> in
        guard let `self` = self else {
          return .just(nil)
        }
        let expiresIn = token?.expiresIn
        let refreshInterval = expiresIn.flatMap { $0 - Constants.tokenExpirationDelta }
        let interval = refreshInterval ?? Constants.tokenRetryDelta
        let delay = Double(max(interval, Constants.tokenRetryDelta))
        return try self.refresh().delay(delay,
                                        scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
      }
      .catchErrorJustReturn(nil)
      .bind(to: tokenVariable).disposed(by: disposeBag)
  }

}

private extension TokenManager {

  func tokenRequest() throws -> Request<SingleResult<TokenModel>> {
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.tokenBaseURLString)
      .set(pathComponents: ["token"])
      .setAuthenticationKey()
      .build()
    return try RequestBuilder()
      .setURLString(URLString)
      .setParams(.json(["grant_type": "refresh_token", "refresh_token": refreshToken]))
      .setMethod(.POST).build()
  }

}
