//
//  AuthenticationManager.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/4/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import Gnomon

private enum Keys {

  static let user = "AuthenticationManagerUserKey"
  static let auth = "AuthenticationManagerAuthKey"

}

final class AuthenticationManager: AuthenticationProvider {

  private let disposeBag = DisposeBag()

  var tokenProvider: TokenProvider?
  private var userProvider: UserProvider?
  let auth: Variable<AuthModel?>
  let user: Variable<UserModel?>

  init(with storage: StorageProvider) {
    user = Variable(storage.value(for: Keys.user))
    auth = Variable(storage.value(for: Keys.auth))
    storage.persist(value: user.asObservable(), for: Keys.user)
    storage.persist(value: auth.asObservable(), for: Keys.auth)
    userProvider = UserManager(with: self)

    auth.asObservable().skipNil().subscribe(onNext: { [weak self] auth in
      self?.tokenProvider = TokenManager(refreshToken: auth.refreshToken)
    }).disposed(by: disposeBag)

    NotificationCenter.default.rx
      .notification(Notification.Name.UIApplicationDidBecomeActive)
      .subscribe(onNext: { [weak self] _ in
        self?.tokenProvider?.invalidate()
      }).disposed(by: disposeBag)
  }

  func login(with username: String, password: String) throws -> Observable<LoginModel> {
    let request = try loginRequest(with: username, password: password)
    let authentication = Gnomon.models(for: request).map { $0.result.model }
      .catchError { error in
        guard case let Gnomon.Error.errorStatusCode(_, data) = error else {
          throw LoginError.undefined
        }
        let errorModel = try LoginModel.model(with: data, atPath: nil)
        return .just(errorModel)
      }
    return try authenticate(for: authentication)
  }

  func logout() {
    auth.value = nil
    user.value = nil
    tokenProvider = nil
  }

  func register(with username: String, password: String) throws -> Observable<RegisterModel> {
    guard let userProvider = userProvider else {
      return .error(APIError.restricted)
    }
    let authentication = userProvider.createUser(username: username, password: password)
    return try authenticate(for: authentication)
  }

  private func authenticate<E>(for authentication: Observable<AuthenticationModel<E>>)
    throws -> Observable<AuthenticationModel<E>> {
    return authentication
      .do(onNext: { [weak self] result in
        guard let `self` = self else {
          return
        }
        self.auth.value = result.auth
      })
  }

  func loadCurrentUser() throws -> Observable<APISingleModel<UserModel>> {
    guard let userProvider = userProvider else {
      throw APIError.restricted
    }
    return userProvider.getUser()
      .do(onNext: { [weak self] result in
        guard let `self` = self else {
          return
        }
        self.user.value = result.model
      })
  }

}

private extension AuthenticationManager {

  func loginRequest(with username: String, password: String) throws -> Request<SingleResult<LoginModel>> {
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.authenticationBaseURLString)
      .set(pathComponents: ["verifyPassword"])
      .setAuthenticationKey()
      .build()
    return try RequestBuilder()
      .setURLString(URLString)
      .setParams(.json(["email": username, "password": password, "returnSecureToken": true]))
      .setMethod(.POST).build()
  }

}
