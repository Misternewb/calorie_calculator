//
//  TokenProvider.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

protocol TokenProvider {

  init(refreshToken: String)

  var token: Observable<TokenModel?> { get }

  func refresh() throws -> Observable<TokenModel?>

  func invalidate()

}
