//
//  UserManager.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/11/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import Gnomon

final class UserManager: UserProvider {

  private weak var authenticationProvider: AuthenticationProvider?
  init(with authenticationProvider: AuthenticationProvider) {
    self.authenticationProvider = authenticationProvider
  }

  func getUsers(cached: Bool) -> Observable<UserMultipleResult> {
    guard let tokenProvider = authenticationProvider?.tokenProvider else {
      return .error(APIError.restricted)
    }

    return tokenProvider.token.flatMap { [weak self] token -> Observable<UserMultipleResult> in
      guard let `self` = self else {
        return .empty()
      }
      guard let token = token else {
        return .error(APIError.undefined)
      }
      let request = try self.getUsersRequest(authToken: token.authToken)
      let result = cached ? Gnomon.cachedThenFetch(request) : Gnomon.models(for: request)
      return result.map({ $0.result.model }).skipNil()
    }
  }

  func getUser() -> Observable<UserSingleResult> {
    guard let tokenProvider = authenticationProvider?.tokenProvider else {
      return .error(APIError.restricted)
    }

    return tokenProvider.token.flatMap { [weak self] token -> Observable<UserSingleResult> in
      guard let `self` = self else {
        return .empty()
      }
      guard let token = token else {
        return .error(APIError.undefined)
      }
      let request = try self.getUserRequest(authToken: token.authToken)
      return Gnomon.models(for: request).map { $0.result.model }
    }
  }

  func createUser(username: String, password: String) -> Observable<RegisterModel> {
    do {
      let request = try self.createUserRequest(with: username, password: password)
      return Gnomon.models(for: request)
        .map { $0.result.model }
        .delay(Params.API.cloudDelay,
               scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
        .catchError { error in
          guard case let Gnomon.Error.errorStatusCode(_, data) = error else {
            throw RegisterError.undefined
          }
          let errorModel = try RegisterModel.model(with: data, atPath: nil)
          return .just(errorModel)
        }
    } catch {
      return .error(error)
    }
  }

  func update(user: UserModel) -> Observable<APIResultModel> {
    guard let tokenProvider = authenticationProvider?.tokenProvider else {
      return .error(APIError.restricted)
    }

    return tokenProvider.token.flatMap { [weak self] token -> Observable<APIResultModel> in
      guard let `self` = self else {
        return .empty()
      }
      guard let token = token else {
        return .error(APIError.undefined)
      }
      let request = try self.updateUserRequest(user: user, authToken: token.authToken)
      return Gnomon.models(for: request).map { $0.result.model }
    }
  }

  func delete(user: UserModel) -> Observable<APIResultModel> {
    guard let tokenProvider = authenticationProvider?.tokenProvider else {
      return .error(APIError.restricted)
    }

    return tokenProvider.token.flatMap { [weak self] token -> Observable<APIResultModel> in
      guard let `self` = self else {
        return .empty()
      }
      guard let token = token else {
        return .error(APIError.undefined)
      }
      let request = try self.deleteUserRequest(user: user, authToken: token.authToken)
      return Gnomon.models(for: request).map { $0.result.model }
        .catchError { error in
          if case DecodingError.dataCorrupted = error {
            return .just(APIResultModel())
          }
          throw error
        }
        .delay(Params.API.cloudDelay,
               scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
    }
  }

}

private extension UserManager {

  var currentUser: UserModel? {
    return authenticationProvider?.user.value
  }

  func getUsersRequest(authToken: String) throws -> Request<SingleOptionalResult<UserMultipleResult>> {
    guard let currentUser = currentUser, currentUser.canFetchUsers else {
      throw "Access denied"
    }
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.databaseBaseURLString)
      .set(authToken: authToken)
      .set(pathComponents: ["users.json"])
      .build()

    return try RequestBuilder()
      .setURLString(URLString)
      .setMethod(.GET).build()
  }

  func getUserRequest(authToken: String) throws -> Request<SingleResult<UserSingleResult>> {
    let userId: String? = currentUser?.id ?? authenticationProvider?.auth.value?.id
    guard let id = userId else {
      throw "Access denied"
    }

    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.databaseBaseURLString)
      .set(pathComponents: ["users", "\(id).json"])
      .set(authToken: authToken)
      .build()
    return try RequestBuilder()
      .setURLString(URLString)
      .setMethod(.GET).build()
  }

  func createUserRequest(with username: String, password: String) throws -> Request<UserCreateResult> {
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.authenticationBaseURLString)
      .set(pathComponents: ["signupNewUser"])
      .setAuthenticationKey()
      .build()
    return try RequestBuilder()
      .setURLString(URLString)
      .setParams(.json(["email": username, "password": password, "returnSecureToken": true]))
      .setMethod(.POST).build()
  }

  func deleteUserRequest(user: UserModel, authToken: String) throws -> Request<SingleResult<APIResultModel>> {
    guard let userId = user.id else {
      throw "Access denied"
    }
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.databaseBaseURLString)
      .set(authToken: authToken)
      .set(pathComponents: ["users", "\(userId).json"])
      .build()

    return try RequestBuilder()
      .setURLString(URLString)
      .setMethod(.DELETE).build()
  }

  func updateUserRequest(user: UserModel,
                         authToken: String) throws -> Request<SingleResult<APIResultModel>> {
    guard let userId = user.id else {
      throw "Access denied"
    }
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.databaseBaseURLString)
      .set(authToken: authToken)
      .set(pathComponents: ["users", "\(userId).json"])
      .build()

    return try RequestBuilder()
      .setURLString(URLString)
      .setParams(.json(user.dictionaryValue))
      .setMethod(.PATCH).build()
  }

}
