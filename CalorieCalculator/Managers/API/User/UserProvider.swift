//
//  UserProvider.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/11/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import Gnomon

protocol UserProvider {

  typealias UserMultipleResult = APIContainerModel<UserModel>
  typealias UserSingleResult = APISingleModel<UserModel>
  typealias UserCreateResult = SingleResult<RegisterModel>

  init(with authenticationProvider: AuthenticationProvider)

  func getUsers(cached: Bool) -> Observable<UserMultipleResult>
  func getUser() -> Observable<UserSingleResult>
  func createUser(username: String, password: String) -> Observable<RegisterModel>
  func update(user: UserModel) -> Observable<APIResultModel>
  func delete(user: UserModel) -> Observable<APIResultModel>

}
