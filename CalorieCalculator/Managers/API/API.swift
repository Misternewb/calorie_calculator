//
//  API.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/4/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Foundation

enum API {

  static let databaseBaseURLString = "https://\(Params.API.id).firebaseio.com"
  static let authenticationBaseURLString = "https://www.googleapis.com/identitytoolkit/v3/relyingparty"
  static let tokenBaseURLString = "https://securetoken.googleapis.com/v1"

}

final class APIURLStringBuilder {

  private var baseURLString: String?
  private var pathComponents = [String]()
  private var parameters = [String: String]()

  @discardableResult
  func set(baseURLString: String) -> Self {
    self.baseURLString = baseURLString
    return self
  }

  @discardableResult
  func set(pathComponents: [String]) -> Self {
    self.pathComponents = pathComponents
    return self
  }

  @discardableResult
  func setParameter(for key: String, value: String) -> Self {
    parameters[key] = value
    return self
  }

  @discardableResult
  func setAuthenticationKey() -> Self {
    return setParameter(for: "key", value: Params.API.key)
  }

  @discardableResult
  func set(authToken: String) -> Self {
    return setParameter(for: "auth", value: authToken)
  }

  func build() throws -> String {
    guard let baseURLString = baseURLString else {
      throw "No base URL"
    }
    let URLString = pathComponents.reduce(baseURLString) { "\($0)/\($1)" }
    guard var components = URLComponents(string: URLString) else {
      throw "Invalid base URL"
    }

    components.queryItems = parameters.map { URLQueryItem(name: $0, value: $1) }

    guard let resultURLString = components.url?.absoluteString else {
      throw "Invalid result URL"
    }
    return resultURLString
  }

}
