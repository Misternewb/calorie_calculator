//
//  MealProvider.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

protocol MealProvider {

  typealias MealMultipleResult = APIContainerModel<MealModel>

  init(with authenticationProvider: AuthenticationProvider)

  func getMeal(for user: UserModel, cached: Bool) -> Observable<MealMultipleResult>
  func create(meal: MealModel, user: UserModel) -> Observable<APIResultModel>
  func update(meal: MealModel, user: UserModel) -> Observable<APIResultModel>
  func delete(meal: MealModel, user: UserModel) -> Observable<APIResultModel>

}
