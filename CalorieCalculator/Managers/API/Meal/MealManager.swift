//
//  MealManager.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift
import Gnomon

final class MealManager: MealProvider {

  fileprivate let authenticationProvider: AuthenticationProvider

  init(with authenticationProvider: AuthenticationProvider) {
    self.authenticationProvider = authenticationProvider
  }

  func getMeal(for user: UserModel, cached: Bool) -> Observable<MealMultipleResult> {
    guard let tokenProvider = authenticationProvider.tokenProvider else {
      return .error(APIError.restricted)
    }

    return tokenProvider.token.flatMap { [weak self] token -> Observable<MealMultipleResult> in
      guard let `self` = self else {
        return .empty()
      }
      guard let token = token else {
        return .error(APIError.undefined)
      }
      let request = try self.getMealsRequest(for: user, authToken: token.authToken)
      let result = cached ? Gnomon.cachedThenFetch(request) : Gnomon.models(for: request)
      return result.map({ $0.result.model }).skipNil()
    }
  }

  func create(meal: MealModel, user: UserModel) -> Observable<APIResultModel> {
    guard let tokenProvider = authenticationProvider.tokenProvider else {
      return .error(APIError.restricted)
    }

    return tokenProvider.token.flatMap { [weak self] token -> Observable<APIResultModel> in
      guard let `self` = self else {
        return .empty()
      }
      guard let token = token else {
        return .error(APIError.undefined)
      }
      let request = try self.createMealRequest(meal: meal, user: user, authToken: token.authToken)
      return Gnomon.models(for: request).map { $0.result.model }
    }
  }

  func update(meal: MealModel, user: UserModel) -> Observable<APIResultModel> {
    guard let tokenProvider = authenticationProvider.tokenProvider else {
      return .error(APIError.restricted)
    }

    return tokenProvider.token.flatMap { [weak self] token -> Observable<APIResultModel> in
      guard let `self` = self else {
        return .empty()
      }
      guard let token = token else {
        return .error(APIError.undefined)
      }
      let request = try self.updateMealRequest(meal: meal, user: user, authToken: token.authToken)
      return Gnomon.models(for: request).map { $0.result.model }
    }
  }

  func delete(meal: MealModel, user: UserModel) -> Observable<APIResultModel> {
    guard let tokenProvider = authenticationProvider.tokenProvider else {
      return .error(APIError.restricted)
    }

    return tokenProvider.token.flatMap { [weak self] token -> Observable<APIResultModel> in
      guard let `self` = self else {
        return .empty()
      }
      guard let token = token else {
        return .error(APIError.undefined)
      }
      let request = try self.deleteMealRequest(meal: meal, authToken: token.authToken)
      return Gnomon.models(for: request).map { $0.result.model }
        .catchError { error in
          if case DecodingError.dataCorrupted = error {
            return .just(APIResultModel())
          }
          throw error
        }
    }
  }

}

private extension MealManager {

  func getMealsRequest(for user: UserModel,
                       authToken: String) throws -> Request<SingleOptionalResult<MealMultipleResult>> {
    guard let userId = user.id else {
      throw "User has no id"
    }
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.databaseBaseURLString)
      .set(authToken: authToken)
      .set(pathComponents: ["meals.json"])
      .setParameter(for: "orderBy", value: "\"/uid\"")
      .setParameter(for: "equalTo", value: "\"\(userId)\"")
      .build()

    return try RequestBuilder()
      .setURLString(URLString)
      .setMethod(.GET).build()
  }

  func createMealRequest(meal: MealModel, user: UserModel,
                         authToken: String) throws -> Request<SingleResult<APIResultModel>> {
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.databaseBaseURLString)
      .set(authToken: authToken)
      .set(pathComponents: ["meals.json"])
      .build()

    var jsonParams = meal.dictionaryValue
    jsonParams["uid"] = user.id

    return try RequestBuilder()
      .setURLString(URLString)
      .setParams(.json(jsonParams))
      .setMethod(.POST).build()
  }

  func deleteMealRequest(meal: MealModel, authToken: String) throws -> Request<SingleResult<APIResultModel>> {
    guard let mealId = meal.id else {
      throw APIError.undefined
    }
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.databaseBaseURLString)
      .set(authToken: authToken)
      .set(pathComponents: ["meals", "\(mealId).json"])
      .build()

    return try RequestBuilder()
      .setURLString(URLString)
      .setMethod(.DELETE).build()
  }

  func updateMealRequest(meal: MealModel, user: UserModel,
                         authToken: String) throws -> Request<SingleResult<APIResultModel>> {
    guard let mealId = meal.id else {
      throw APIError.undefined
    }
    let URLString = try APIURLStringBuilder()
      .set(baseURLString: API.databaseBaseURLString)
      .set(authToken: authToken)
      .set(pathComponents: ["meals", "\(mealId).json"])
      .build()

    return try RequestBuilder()
      .setURLString(URLString)
      .setParams(.json(meal.dictionaryValue))
      .setMethod(.PATCH).build()
  }

}
