//
//  UserUpdateProvider.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/11/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

protocol UserUpdateProvider {

  var user: Variable<UserModel?> { get }

  init(with user: UserModel?, userProvider: UserProvider)

  func update(with user: UserModel?)

  func startUpdating()
  func stopUpdating()

}
