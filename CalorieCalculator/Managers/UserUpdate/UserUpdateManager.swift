//
//  UserUpdateManager.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/11/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

private enum Constants {

  static let uploadPeriod: TimeInterval = 2
  static let refreshPeriod: TimeInterval = 10

}

final class UserUpdateManager: UserUpdateProvider {

  private var disposeBag = DisposeBag()
  private var refreshDisposeBag = DisposeBag()

  let user: Variable<UserModel?>
  private let userProvider: UserProvider

  init(with user: UserModel?, userProvider: UserProvider) {
    self.user = Variable(user)
    self.userProvider = userProvider
    startUpdating()
  }

  func update(with user: UserModel?) {
    self.user.value = user
  }

  func startUpdating() {
    user.asObservable()
      .skipNil().distinctUntilChanged().skip(1)
      .debounce(Constants.uploadPeriod,
                scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
      .flatMap { [weak self] user -> Observable<APIResultModel> in
        guard let `self` = self else {
          return .empty()
        }
        self.startRefreshing()
        return self.userProvider.update(user: user)
      }
      .catchErrorJustReturn(APIResultModel())
      .subscribe().disposed(by: disposeBag)

    startRefreshing()
  }

  private func startRefreshing() {
    refreshDisposeBag = DisposeBag()
    Observable<Int>.interval(Constants.refreshPeriod,
                             scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
      .flatMap { [weak self] _ -> Observable<UserModel> in
        guard let `self` = self else {
          return .empty()
        }
        return self.userProvider.getUser()
          .map { $0.model }
          .catchErrorJustReturn(nil).skipNil()
      }
      .bind(to: self.user).disposed(by: refreshDisposeBag)
  }

  func stopUpdating() {
    disposeBag = DisposeBag()
    refreshDisposeBag = DisposeBag()
  }

}
