//
//  StorageProvider.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/9/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Foundation
import RxSwift

protocol StorageProvider {

  func value<T: Codable>(for key: String) -> T?
  func persist<T: Codable>(value: Observable<T>, for key: String)

}
