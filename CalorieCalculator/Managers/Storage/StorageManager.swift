//
//  StorageManager.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/9/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

final class StorageManager: StorageProvider {

  private let userDefaults = UserDefaults.standard
  private let disposeBag = DisposeBag()

  func value<T: Codable>(for key: String) -> T? {
    return userDefaults.codableValue(forKey: key)
  }

  func persist<T: Codable>(value: Observable<T>, for key: String) {
    value.subscribe(onNext: { [weak self] value in
      self?.userDefaults.setCodable(value, forKey: key)
    }).disposed(by: disposeBag)
  }

}
