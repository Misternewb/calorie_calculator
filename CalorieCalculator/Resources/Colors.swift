// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit
  enum Colors { }
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit
  enum Colors { }
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable identifier_name line_length type_body_length
extension Colors {
  /// 0x605f5eff (r: 96, g: 95, b: 94, a: 255)
  static let darkGray = #colorLiteral(red: 0.376471, green: 0.372549, blue: 0.368627, alpha: 1.0)
  /// 0x323232ff (r: 50, g: 50, b: 50, a: 255)
  static let darkText = #colorLiteral(red: 0.196078, green: 0.196078, blue: 0.196078, alpha: 1.0)
  /// 0x2ed573ff (r: 46, g: 213, b: 115, a: 255)
  static let flatGreen = #colorLiteral(red: 0.180392, green: 0.835294, blue: 0.45098, alpha: 1.0)
  /// 0xff4757ff (r: 255, g: 71, b: 87, a: 255)
  static let flatRed = #colorLiteral(red: 1.0, green: 0.278431, blue: 0.341176, alpha: 1.0)
  /// 0xf5f5f5ff (r: 245, g: 245, b: 245, a: 255)
  static let grayBackground = #colorLiteral(red: 0.960784, green: 0.960784, blue: 0.960784, alpha: 1.0)
  /// 0x22a6b3ff (r: 34, g: 166, b: 179, a: 255)
  static let roleAdmin = #colorLiteral(red: 0.133333, green: 0.65098, blue: 0.701961, alpha: 1.0)
  /// 0x4834d4ff (r: 72, g: 52, b: 212, a: 255)
  static let roleManager = #colorLiteral(red: 0.282353, green: 0.203922, blue: 0.831373, alpha: 1.0)
  /// 0x130f40ff (r: 19, g: 15, b: 64, a: 255)
  static let roleUser = #colorLiteral(red: 0.0745098, green: 0.0588235, blue: 0.25098, alpha: 1.0)
  /// 0x785eddff (r: 120, g: 94, b: 221, a: 255)
  static let tint = #colorLiteral(red: 0.470588, green: 0.368627, blue: 0.866667, alpha: 1.0)
}
// swiftlint:enable identifier_name line_length type_body_length
