// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
enum L10n {
  /// Cal Cal
  static let applicationName = L10n.tr("Localizable", "application_name")

  enum Api {

    enum Errors {
      /// You are not allowed to do that.
      static let restricted = L10n.tr("Localizable", "api.errors.restricted")
      /// Something went wrong, please try again!
      static let undefined = L10n.tr("Localizable", "api.errors.undefined")
    }
  }

  enum Authentication {
    /// Confirm password
    static let confirmPasswordPlaceholder = L10n.tr("Localizable", "authentication.confirm_password_placeholder")
    /// Already have account? Sign in!
    static let haveAccountPromotion = L10n.tr("Localizable", "authentication.have_account_promotion")
    /// Do not have account? Register!
    static let noAccountPromotion = L10n.tr("Localizable", "authentication.no_account_promotion")
    /// Password
    static let passwordPlaceholder = L10n.tr("Localizable", "authentication.password_placeholder")
    /// Login
    static let submitLogin = L10n.tr("Localizable", "authentication.submit_login")
    /// Register
    static let submitRegister = L10n.tr("Localizable", "authentication.submit_register")
    /// Please, login.
    static let subtitleLogin = L10n.tr("Localizable", "authentication.subtitle_login")
    /// Please, register.
    static let subtitleRegister = L10n.tr("Localizable", "authentication.subtitle_register")
    /// Welcome to Cal Cal!
    static let title = L10n.tr("Localizable", "authentication.title")
    /// Email
    static let usernamePlaceholder = L10n.tr("Localizable", "authentication.username_placeholder")

    enum InputErrors {
      /// Invalid email! Please try another one!
      static let invalidEmail = L10n.tr("Localizable", "authentication.input_errors.invalid_email")
      /// Passwords do not match!
      static let invalidRepeatPassword = L10n.tr("Localizable", "authentication.input_errors.invalid_repeat_password")
      /// Password too long, please try shorter one!
      static let longPassword = L10n.tr("Localizable", "authentication.input_errors.long_password")
      /// Password too short, please try longer one!
      static let shortPassword = L10n.tr("Localizable", "authentication.input_errors.short_password")
    }

    enum LoginErrors {
      /// Invalid email!
      static let invalidEmail = L10n.tr("Localizable", "authentication.login_errors.invalid_email")
      /// Invalid password!
      static let invalidPassword = L10n.tr("Localizable", "authentication.login_errors.invalid_password")
      /// Something went wrong, please try again!
      static let undefined = L10n.tr("Localizable", "authentication.login_errors.undefined")
      /// User is disabled by administrator.
      static let userDisabled = L10n.tr("Localizable", "authentication.login_errors.user_disabled")
    }

    enum RegisterErrors {
      /// Email already exists!
      static let invalidEmail = L10n.tr("Localizable", "authentication.register_errors.invalid_email")
      /// Registration is disabled!
      static let registrationDisabled = L10n.tr("Localizable", "authentication.register_errors.registration_disabled")
      /// Something went wrong, please try again!
      static let undefined = L10n.tr("Localizable", "authentication.register_errors.undefined")
      /// Your account is blocked for some time.
      static let userBlocked = L10n.tr("Localizable", "authentication.register_errors.user_blocked")
    }
  }

  enum Common {

    enum Actions {
      /// Cancel
      static let cancel = L10n.tr("Localizable", "common.actions.cancel")
      /// Create
      static let create = L10n.tr("Localizable", "common.actions.create")
      /// Delete
      static let delete = L10n.tr("Localizable", "common.actions.delete")
      /// Update
      static let update = L10n.tr("Localizable", "common.actions.update")
    }

    enum LoaderContainer {

      enum Errors {
        /// Something went wrong, please try again!
        static let `default` = L10n.tr("Localizable", "common.loader_container.errors.default")
        /// There is currently no data.
        static let noData = L10n.tr("Localizable", "common.loader_container.errors.no_data")
      }
    }
  }

  enum Filter {
    /// Start day
    static let fromDate = L10n.tr("Localizable", "filter.from_date")
    /// Start time
    static let fromTime = L10n.tr("Localizable", "filter.from_time")
    /// Filter
    static let title = L10n.tr("Localizable", "filter.title")
    /// End day
    static let toDate = L10n.tr("Localizable", "filter.to_date")
    /// End time
    static let toTime = L10n.tr("Localizable", "filter.to_time")
  }

  enum Home {

    enum Tabs {
      /// Management
      static let management = L10n.tr("Localizable", "home.tabs.management")
      /// Meals
      static let meals = L10n.tr("Localizable", "home.tabs.meals")
      /// Settings
      static let settings = L10n.tr("Localizable", "home.tabs.settings")
    }
  }

  enum Meal {
    /// calories
    static let calories = L10n.tr("Localizable", "meal.calories")
  }

  enum MealEditor {
    /// New meal
    static let createTitle = L10n.tr("Localizable", "meal_editor.create_title")
    /// Update meal
    static let updateTitle = L10n.tr("Localizable", "meal_editor.update_title")

    enum Errors {
      /// Meal name must not be empty!
      static let emptyMealName = L10n.tr("Localizable", "meal_editor.errors.empty_meal_name")
    }
  }

  enum MealList {
    /// Meals
    static let title = L10n.tr("Localizable", "meal_list.title")

    enum Errors {
      /// You do not have any meal right now, tap + to add some!
      static let noData = L10n.tr("Localizable", "meal_list.errors.no_data")
    }
  }

  enum Settings {
    /// Your daily calories threshold: 
    static let dailyCaloriesTitle = L10n.tr("Localizable", "settings.daily_calories_title")
    /// Logout
    static let logout = L10n.tr("Localizable", "settings.logout")
    /// Settings
    static let title = L10n.tr("Localizable", "settings.title")
  }

  enum User {
    /// daily calories threshold
    static let dailyCaloriesThreshold = L10n.tr("Localizable", "user.daily_calories_threshold")

    enum Role {
      /// Admin
      static let admin = L10n.tr("Localizable", "user.role.admin")
      /// Manager
      static let manager = L10n.tr("Localizable", "user.role.manager")
      /// User
      static let user = L10n.tr("Localizable", "user.role.user")
    }
  }

  enum UserEditor {
    /// New user
    static let createTitle = L10n.tr("Localizable", "user_editor.create_title")
    /// Daily calories threshold: 
    static let dailyCaloriesTitle = L10n.tr("Localizable", "user_editor.daily_calories_title")
    /// Update user
    static let updateTitle = L10n.tr("Localizable", "user_editor.update_title")

    enum Errors {
      /// Password is incorrect!
      static let wrongPassword = L10n.tr("Localizable", "user_editor.errors.wrong_password")
      /// Email is incorrect!
      static let wrongUsername = L10n.tr("Localizable", "user_editor.errors.wrong_username")
    }
  }

  enum UserList {
    /// Users
    static let title = L10n.tr("Localizable", "user_list.title")

    enum Errors {
      /// There are no users right now, tap + to add some!
      static let noData = L10n.tr("Localizable", "user_list.errors.no_data")
    }
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
