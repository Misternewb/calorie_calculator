//
// fastlane generated file
//

enum Params {

  enum API {
    static let key = "AIzaSyAmOdqZNc3dUzIAujKc_KHP77c8sbAydQ4"
    static let id = "calcal-2acc7"
    static let cloudDelay = 2.5
  }

  enum Meal {
    static let minCaloryCount = 0
    static let maxCaloryCount = 6000
    static let defaultCaloryCount = 200
  }

  enum Settings {
    static let minCaloryThreshold = 1000
    static let maxCaloryThreshold = 10000
    static let defaultCaloryThreshold = 2200
  }

}
