//
//  AppConfigurator.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/4/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Foundation
import Gnomon

final class AppConfigurator {

  private let router: AuthenticationRouter

  init(window: UIWindow) {
    router = AuthenticationRouter(with: AppDelegate.shared.providers.authentication,
                                  window: window)
    window.tintColor = Colors.tint
    configure()
  }

  func configure() {
    Gnomon.logging = true
  }

}
