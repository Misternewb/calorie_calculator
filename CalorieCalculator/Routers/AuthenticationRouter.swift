//
//  AuthenticationRouter.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import RxSwift

final class AuthenticationRouter {

  private let authenticationProvider: AuthenticationProvider
  private let disposeBag = DisposeBag()

  init(with authenticationProvider: AuthenticationProvider, window: UIWindow) {
    self.authenticationProvider = authenticationProvider
    let rootViewController = UIViewController()
    window.rootViewController = rootViewController
    window.makeKeyAndVisible()

    authenticationProvider.user.asDriver()
      .distinctUntilChanged { $0?.id == $1?.id && $0?.role == $1?.role }
      .map { user -> UIViewController in
        if let user = user {
          return HomeController(with: user)
        } else {
          return AuthenticationController()
        }
      }
      .drive(onNext: { controller in
        let animated = window.rootViewController != rootViewController
        UIView.transition(with: window,
                          duration: animated ? 0.3 : 0,
                          options: .transitionCrossDissolve,
                          animations: {
          window.rootViewController = controller
        }, completion: nil)
      }).disposed(by: disposeBag)
  }

}
