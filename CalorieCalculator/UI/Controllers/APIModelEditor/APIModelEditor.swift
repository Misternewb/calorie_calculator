//
//  APIModelEditor.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import RxSwift
import SDCAlertView

extension APIModelEditor {

  indirect enum Mode<T> {

    case create
    case update(T)

    fileprivate var submitTitle: String {
      switch self {
      case .create: return L10n.Common.Actions.create
      case .update: return L10n.Common.Actions.update
      }
    }

    var model: T? {
      switch self {
      case .create: return nil
      case let .update(model): return model
      }
    }

  }

}

class APIModelEditor<Model, T: Reusable & View> where T.Data == Model? {

  typealias EditorMode = Mode<Model>

  let submitSubject = PublishSubject<Model>()

  let mode: EditorMode
  init(with mode: EditorMode) {
    self.mode = mode
  }

  var controllerTitle: String? {
    return nil
  }

  func buildModel(from view: T) throws -> Model {
    throw "You have to override this method"
  }

  final func buildController() -> UIViewController {
    let alert = AlertController(title: controllerTitle,
                                message: nil,
                                preferredStyle: .alert)
    let view = T.init()

    view.setup(with: mode.model)
    alert.contentView.addSubview(view)
    view.snp.makeConstraints {
      $0.edges.equalToSuperview()
    }
    let submitAction = AlertAction(title: mode.submitTitle, style: .preferred)
    let cancelAction = AlertAction(title: L10n.Common.Actions.cancel, style: .destructive)
    alert.contentView.invalidateIntrinsicContentSize()
    alert.addAction(submitAction)
    alert.addAction(cancelAction)
    alert.shouldDismissHandler = { action in
      guard action == submitAction else {
        return true
      }
      do {
        let model = try self.buildModel(from: view)
        self.submitSubject.onNext(model)
        return true
      } catch {
        alert.showError(with: error.localizedDescription)
        return false
      }
    }
    return alert
  }

}
