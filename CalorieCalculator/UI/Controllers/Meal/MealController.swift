//
//  MealController.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import RxSwift
import PKHUD

class MealController: LoaderTableViewController {

  private typealias MealCell = TableCell<MealCellView>
  private typealias HeaderSection = TableHeaderSection<MealDayHeaderView>

  fileprivate let viewModel: MealViewModel
  let user: Variable<UserModel>

  init(with user: UserModel) {
    self.user = Variable(user)
    viewModel = MealViewModel(with: user,
                              authenticationProvider: AppDelegate.shared.providers.authentication)
    super.init()
  }

  required init() {
    fatalError("init() has not been implemented")
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func loadView() {
    super.loadView()
    noDataLabel.text = L10n.MealList.Errors.noData
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = L10n.MealList.title
    setupNavigationItems()

    viewModel.filter.asDriver().distinctUntilChanged({ $0 == $1 }).skip(1)
      .drive(onNext: { [weak self] _ in
      self?.source.forceReloadData(keepCurrentDataBeforeUpdate: false)
    }).disposed(by: disposeBag)
  }

  override func performLoading(intent: LoaderIntent) -> SectionObservable? {
    return viewModel.loadData(for: intent)
      .observeOn(MainScheduler.instance)
      .map { [weak self] dayMeals -> [Sectionable] in
        let sortedDays = dayMeals.keys.sorted(by: { $0 > $1 })
        return sortedDays.compactMap { day -> Sectionable? in
          guard let viewModels = dayMeals[day] else {
            return nil
          }

          if let disposeBag = self?.disposeBag {
            viewModels.forEach {
              $0.deleteSubject.subscribe(onNext: {
                self?.delete(meal: $0)
              }).disposed(by: disposeBag)

              $0.updateSubject.subscribe(onNext: {
                self?.update(meal: $0)
              }).disposed(by: disposeBag)
            }
          }
          guard let `self` = self else {
            return nil
          }
          let cells = viewModels.map { MealCell(data: $0) }
          let mealDayViewModel = MealDayViewModel(with: self.user.asObservable(), date: day,
                                                  meals: viewModels.map { $0.meal })
          return HeaderSection(cells: cells, headerData: mealDayViewModel)
        }
      }
  }

}

private extension MealController {

  func delete(meal: MealModel) {
    HUD.show(.progress)
    let delete = viewModel.mealProvider.delete(meal: meal, user: user.value)
    handleModifyMeal(modify: delete)
  }

  func update(meal: MealModel) {
    presentMealEditController(with: .update(meal))
  }

  func handleModifyMeal(modify: Observable<APIResultModel>) {
    modify.observeOn(MainScheduler.instance)
      .do(onNext: { [weak self] _ in
        HUD.hide()
        self?.source.forceReloadData(keepCurrentDataBeforeUpdate: true)
      })
      .subscribe(onError: { [weak self] error in
        HUD.hide()
        self?.showError(with: error.localizedDescription)
      }).disposed(by: disposeBag)
  }

}

private extension MealController {

  func setupNavigationItems() {
    let createBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil)
    createBarButton.rx.tap.asDriver().drive(onNext: { [weak self] in
      self?.presentMealEditController(with: .create)
    }).disposed(by: disposeBag)

    let filterBarButton = UIBarButtonItem(image: Asset.Controls.filter.image,
                                          style: .plain, target: nil, action: nil)
    viewModel.filter.asDriver().map { $0 == nil ? Colors.darkGray : Colors.tint }
      .drive(onNext: { color in
        filterBarButton.tintColor = color
      }).disposed(by: disposeBag)
    filterBarButton.rx.tap.asDriver().drive(onNext: { [weak self] in
      self?.showFilter()
    }).disposed(by: disposeBag)
    navigationItem.rightBarButtonItems = [createBarButton, filterBarButton]
  }

}

private extension MealController {

  func showFilter() {
    let filterController = FilterController(with: viewModel.filter.value)
    filterController.filter.asDriver(onErrorJustReturn: nil)
      .drive(viewModel.filter).disposed(by: disposeBag)
    navigationController?.present(filterController.inNavigation(), animated: true, completion: nil)
  }

  func presentMealEditController(with mode: MealEditor.EditorMode) {
    let editor = MealEditor(with: mode)
    let controller = editor.buildController()
    present(controller, animated: true, completion: nil)
    let editing = editor.submitSubject
      .do(onNext: { _ in
        HUD.show(.progress)
      })
      .flatMap { [weak self] meal -> Observable<APIResultModel> in
        guard let `self` = self else {
          return .empty()
        }
        switch mode {
        case .create:
          return self.viewModel.mealProvider.create(meal: meal, user: self.user.value)
        case .update:
          return self.viewModel.mealProvider.update(meal: meal, user: self.user.value)
        }
      }
    handleModifyMeal(modify: editing)
  }

}
