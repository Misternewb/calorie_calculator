//
//  MealEditor.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import SDCAlertView
import RxSwift

final class MealEditor: APIModelEditor<MealModel, MealView> {

  enum EditorError: LocalizedError {

    case emptyMealName

    var errorDescription: String? {
      return L10n.MealEditor.Errors.emptyMealName
    }

  }

  override var controllerTitle: String? {
    switch mode {
    case .create: return L10n.MealEditor.createTitle
    case .update: return L10n.MealEditor.updateTitle
    }
  }

  override func buildModel(from view: MealView) throws -> MealModel {
    guard let name = view.nameTextField.text, name.count > 0 else {
      throw EditorError.emptyMealName
    }

    let date = view.datePicker.date
    let calories = view.caloriesPicker.calories
    let meal = MealModel(with: mode.model?.id, name: name,
                         date: date, calories: calories)
    return meal
  }

}
