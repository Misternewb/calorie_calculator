//
//  FilterController.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/13/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import RxSwift

private enum Constants {

  static let sectionIndex = 0

}

class FilterController: TableViewController {

  private typealias Cell = TableCell<FilterSwitchCellView>

  let filter = PublishSubject<MealFilterModel?>()

  let viewModel: FilterViewModel

  init(with filter: MealFilterModel?) {
    viewModel = FilterViewModel(with: filter)
    super.init()
  }

  required init() {
    viewModel = FilterViewModel(with: nil)
    super.init()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func loadView() {
    super.loadView()

    let viewModels: [FilterCellViewModel] = [
      viewModel.fromDateViewModel,
      viewModel.toDateViewModel,
      viewModel.fromTimeViewModel,
      viewModel.toTimeViewModel
    ]

    viewModels.forEach { viewModel in
      viewModel.selected.asDriver().distinctUntilChanged().drive(onNext: { [weak self] _ in
        guard let `self` = self, self.sections.count > 0 else {
          return
        }

        guard let rowIndex = self.sections[Constants.sectionIndex].cells
          .index(where: { $0.id == viewModel.type.id }) else {
          return
        }
        let indexPath = IndexPath(row: rowIndex, section: Constants.sectionIndex)
        self.containerView.beginUpdates()
        self.containerView.reloadRows(at: [indexPath],
                                      with: .automatic)
        self.containerView.endUpdates()
        self.containerView.scrollToRow(at: indexPath, at: .none, animated: true)
      }).disposed(by: disposeBag)
    }

    let cells = viewModels.map { Cell(data: $0, id: $0.type.id)}
    source.sections = [Section(cells: cells)]
    containerView.reloadData()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = L10n.Filter.title
    setupNavigationItems()
  }

}

private extension FilterController {

  func setupNavigationItems() {
    let closeBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
    closeBarButton.rx.tap.asDriver().drive(onNext: { [weak self] in
      guard let `self` = self else {
        return
      }
      let filter = self.viewModel.buildFilter()
      self.filter.onNext(filter)
      self.dismiss(animated: true, completion: nil)
    }).disposed(by: disposeBag)

    let cancelBarButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: nil)
    cancelBarButton.rx.tap.asDriver().drive(onNext: { [weak self] in
      guard let `self` = self else {
        return
      }
      self.filter.onNext(nil)
      self.dismiss(animated: true, completion: nil)
    }).disposed(by: disposeBag)

    navigationItem.leftBarButtonItem = closeBarButton
    navigationItem.rightBarButtonItem = cancelBarButton
  }

}
