//
//  UserEditor.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import SDCAlertView
import RxSwift

final class UserEditor: APIModelEditor<UserModel, UserView> {

  private let user: UserModel
  init(with mode: EditorMode, user: UserModel) {
    self.user = user
    super.init(with: mode)
  }

  enum EditorError: LocalizedError {

    case wrongUsername, wrongPassword

    var errorDescription: String? {
      switch self {
      case .wrongUsername:
        return L10n.UserEditor.Errors.wrongUsername
      case .wrongPassword:
        return L10n.UserEditor.Errors.wrongPassword
      }
    }

  }

  private let viewModel = AuthenticationViewModel(with: AppDelegate.shared.providers.authentication)

  override var controllerTitle: String? {
    switch mode {
    case .create: return L10n.UserEditor.createTitle
    case .update: return L10n.UserEditor.updateTitle
    }
  }

  override func buildModel(from view: UserView) throws -> UserModel {
    viewModel.mode.value = .register

    let userRole = view.userRoleView.selectedRole
    let caloryThreshold = view.calorySelectionView.caloryThreshold
    switch mode {
    case .update(let user):
      return UserModel(with: user.id, role: userRole,
                       caloriesThreshold: caloryThreshold, email: user.email)
    case .create:
      let name = view.nameTextField.text ?? ""
      let password = view.passwordTextField.text ?? ""
      let repeatPassword = view.repeatPasswordTextField.text ?? ""
      guard viewModel.validate(email: name) else {
        throw EditorError.wrongUsername
      }

      let passwordValidity = viewModel.validate(password: password)
      guard case .valid = passwordValidity else {
        throw EditorError.wrongPassword
      }

      guard viewModel.validate(password: password,
                               repeatPassword: repeatPassword) else {
        throw EditorError.wrongPassword
      }

      return UserModel(with: nil, role: userRole, caloriesThreshold: caloryThreshold,
                       email: name, password: password)
    }
  }

}
