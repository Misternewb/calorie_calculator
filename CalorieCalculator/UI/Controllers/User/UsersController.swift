//
//  UsersController.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import RxSwift
import PKHUD

class UsersController: LoaderTableViewController {

  private typealias UserCell = TableCell<UserCellView>
  private typealias HeaderSection = TableHeaderSection<UserRoleHeaderView>

  let viewModel: UserViewModel
  let user: UserModel

  init(with user: UserModel) {
    self.user = user
    viewModel = UserViewModel(user: user,
                              authenticationProvider: AppDelegate.shared.providers.authentication)
    super.init()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  required init() {
    fatalError("init() has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    title = L10n.UserList.title
    setupNavigationItems()
  }

  override func loadView() {
    super.loadView()

    noDataLabel.text = L10n.UserList.Errors.noData
  }

  override func performLoading(intent: LoaderIntent) -> SectionObservable? {
    return viewModel.loadData(for: intent)
      .observeOn(MainScheduler.instance)
      .map { [weak self] datasource -> [Sectionable] in
        let roleOrder = UserModel.Role.allValues
        return roleOrder.compactMap { role -> Sectionable? in
          guard let viewModels = datasource[role] else {
            return nil
          }

          if let disposeBag = self?.disposeBag {
            viewModels.forEach {
              $0.deleteSubject.subscribe(onNext: {
                self?.delete(user: $0)
              }).disposed(by: disposeBag)

              $0.updateSubject.subscribe(onNext: {
                self?.update(user: $0)
              }).disposed(by: disposeBag)
            }
          }

          let cells = viewModels.sorted(by: { $0.user.email < $1.user.email })
            .map { viewModel -> Cellable in
              UserCell(data: viewModel) {
                guard viewModel.user.canHaveMeals else {
                  return
                }
                let mealController = MealController(with: viewModel.user)
                self?.navigationController?.pushViewController(mealController, animated: true)
              }
            }
          return HeaderSection(cells: cells, headerData: role)
        }
    }
  }

}

private extension UsersController {

  func setupNavigationItems() {
    let createBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil)
    createBarButton.rx.tap.asDriver().drive(onNext: { [weak self] in
      self?.presentUserEditController(with: .create)
    }).disposed(by: disposeBag)
    navigationItem.rightBarButtonItem = createBarButton
  }

}

private extension UsersController {

  func presentUserEditController(with mode: UserEditor.EditorMode) {
    let editor = UserEditor(with: mode, user: user)
    let controller = editor.buildController()
    present(controller, animated: true, completion: nil)
    let userProvider = viewModel.userProvider

    let submit = editor.submitSubject.do(onNext: { _ in
      HUD.show(.progress)
    })

    let modify: Observable<Void>
    switch mode {
    case .create:
      modify = submit
        .flatMap { user -> Observable<UserModel> in
          userProvider.createUser(username: user.email,
                                                 password: user.password ?? "")
            .map {
              UserModel(with: $0.auth.id, role: user.role,
                        caloriesThreshold: user.caloriesThreshold, email: user.email)
            }
        }
        .flatMap { user -> Observable<Void> in
          userProvider.update(user: user).map { _ in () }
        }
    case .update:
      modify = submit
        .flatMap { user -> Observable<APIResultModel> in
          userProvider.update(user: user)
        }
        .map { _ in () }
    }
    handleModifyUser(modify: modify)
  }

}

private extension UsersController {

  func delete(user: UserModel) {
    HUD.show(.progress)
    let delete = viewModel.userProvider.delete(user: user).map { _ in () }
    handleModifyUser(modify: delete)
  }

  func update(user: UserModel) {
    presentUserEditController(with: .update(user))
  }

  func handleModifyUser(modify: Observable<Void>) {
    modify.observeOn(MainScheduler.instance)
      .do(onNext: { [weak self] in
        HUD.hide()
        self?.source.forceReloadData(keepCurrentDataBeforeUpdate: true)
      })
      .subscribe(onError: { [weak self] error in
        HUD.hide()
        self?.showError(with: error.localizedDescription)
      }).disposed(by: disposeBag)
  }

}
