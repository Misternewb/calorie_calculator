//
//  LoaderController.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import NVActivityIndicatorView

class LoaderViewController<View: UIScrollView & AccessorView>: ContainerViewController<View>, Loader
where View.Source: LoaderReusableSource {

  let activityIndicator = NVActivityIndicatorView(frame: .zero,
                                                  type: .ballRotateChase,
                                                  color: Colors.tint)

  let errorLabel = UILabel.defaultInfoLabel(with: L10n.Common.LoaderContainer.Errors.default)

  let noDataLabel = UILabel.defaultInfoLabel(with: L10n.Common.LoaderContainer.Errors.noData)

  let refreshControl = UIRefreshControl.new {
    $0.tintColor = Colors.tint
  }

  override func loadView() {
    super.loadView()

    containerView.source.loader = self

    refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    containerView.addSubview(refreshControl)
    view.addSubviews([activityIndicator, noDataLabel, errorLabel])
    activityIndicator.snp.makeConstraints { make in
      make.center.equalToSuperview()
      make.size.equalTo(50)
    }

    noDataLabel.snp.makeConstraints { make in
      make.center.equalToSuperview()
      make.width.lessThanOrEqualToSuperview()
    }

    errorLabel.snp.makeConstraints { make in
      make.center.equalToSuperview()
      make.width.lessThanOrEqualToSuperview()
    }

    setupSourceCallbacks()
  }

  private func setupSourceCallbacks() {
    source.startProgress = { [weak self] intent in
      guard let `self` = self else { return }

      switch intent {
      case .initial, .force:
        self.activityIndicator.superview?.bringSubview(toFront: self.activityIndicator)
        self.activityIndicator.startAnimating()
      default: break
      }
    }

    source.stopProgress = { [weak self] intent in
      switch intent {
      case .initial, .force:
        self?.activityIndicator.stopAnimating()
      case .pullToRefresh:
        self?.refreshControl.endRefreshing()
      default: break
      }
    }

    source.updateEmptyView = { [weak self] state in
      guard let strongSelf = self else { return }

      switch state {
      case .empty:
        strongSelf.noDataLabel.isHidden = false
      case .error:
        strongSelf.noDataLabel.isHidden = true
      default:
        strongSelf.noDataLabel.isHidden = true
        strongSelf.errorLabel.isHidden = true
      }
    }
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    containerView.source.appear()
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)

    containerView.source.disappear()
  }

  func performLoading(intent: LoaderIntent) -> SectionObservable? {
    return nil
  }

  @objc private func pullToRefresh() {
    source.pullToRefresh()
  }

}

private extension UILabel {

  static func defaultInfoLabel(with text: String) -> UILabel {
    return UILabel.new {
      $0.font = Fonts.Lato.bold.font(size: 22)
      $0.textColor = Colors.darkText
      $0.text = text
      $0.textAlignment = .center
      $0.isHidden = true
      $0.numberOfLines = 0
    }
  }

}

class GenericLoaderCollectionViewController<T: LoaderReusableSource>: LoaderViewController<CollectionView<T>>
where T.Container == UICollectionView {

  override func loadView() {
    super.loadView()
    containerView.collectionViewLayout = collectionViewLayout()
  }

  func collectionViewLayout() -> UICollectionViewFlowLayout {
    let layout = UICollectionViewFlowLayout()
    layout.minimumLineSpacing = 0
    layout.minimumInteritemSpacing = 0
    return layout
  }

}

// swiftlint:disable:next line_length
class LoaderCollectionViewController: GenericLoaderCollectionViewController<LoaderDecoratorSource<CollectionViewSource>> {}

class GenericLoaderTableViewController<T: LoaderReusableSource>: LoaderViewController<TableView<T>>
where T.Container == UITableView {}

class LoaderTableViewController: GenericLoaderTableViewController<LoaderDecoratorSource<TableViewSource>> {}
