//
//  NavigationController.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NavigationController: UINavigationController {

  fileprivate let pop = PublishSubject<Void>()
  fileprivate let push = PublishSubject<Void>()
  private let disposeBag = DisposeBag()

  override func viewDidLoad() {
    super.viewDidLoad()

    let willShow = rx.willShow.asObservable().map { [unowned self] event -> Bool in
      return self.viewControllers.contains(event.viewController)
      }.publish()

    willShow.filter { $0 }.map { _ in () }.bind(to: pop).disposed(by: disposeBag)
    willShow.filter { !$0 }.map { _ in () }.bind(to: push).disposed(by: disposeBag)

    willShow.connect().disposed(by: disposeBag)
  }

  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .portrait
  }

  override var shouldAutorotate: Bool {
    return false
  }

}

extension Reactive where Base: NavigationController {

  var willPop: Observable<Void> {
    return base.pop
  }

  var willPush: Observable<Void> {
    return base.push
  }

}
