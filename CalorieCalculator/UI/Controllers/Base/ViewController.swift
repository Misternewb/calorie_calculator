//
//  ViewController.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {

  class func createInNavigation() -> NavigationController {
    return NavigationController(rootViewController: self.init())
  }

  let disposeBag = DisposeBag()

  required init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError()
  }

  var baseNavigationController: NavigationController? {
    return navigationController as? NavigationController
  }

  override var childViewControllerForStatusBarStyle: UIViewController? { return presentedViewController }
  override var preferredStatusBarStyle: UIStatusBarStyle { return .default }

  var showNavigationBackground: Bool { return true }

  override func loadView() {
    super.loadView()

    view.backgroundColor = .white
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    edgesForExtendedLayout = [.top]
  }

  var size = CGSize.zero

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    size = view.frame.size
  }

}
