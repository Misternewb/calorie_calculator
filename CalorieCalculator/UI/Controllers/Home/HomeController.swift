//
//  HomeController.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import RxSwift

class HomeController: UITabBarController {

  private let disposeBag = DisposeBag()

  fileprivate let user: UserModel

  init(with user: UserModel) {
    self.user = user
    super.init(nibName: nil, bundle: nil)
  }

  required init() {
    fatalError("init() has not been implemented")
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func loadView() {
    super.loadView()

    let mealController = MealController(with: user)
    let meals = mealController.inNavigation()
    AppDelegate.shared.providers.authentication.user.asObservable()
      .skipNil().skip(1)
      .bind(to: mealController.user).disposed(by: disposeBag)
    meals.tabBarItem = UITabBarItem(title: L10n.Home.Tabs.meals,
                                    image: Asset.Tabs.meals.image,
                                    selectedImage: nil)
    let settings = SettingsController(with: user).inNavigation()
    settings.tabBarItem = UITabBarItem(title: L10n.Home.Tabs.settings,
                                       image: Asset.Tabs.settings.image,
                                       selectedImage: nil)

    let users = UsersController(with: user).inNavigation()
    users.tabBarItem = UITabBarItem(title: L10n.Home.Tabs.management,
                                       image: Asset.Tabs.manage.image,
                                       selectedImage: nil)
    let controllers: [UIViewController?] = [
      user.canHaveMeals ? meals : nil,
      user.canFetchUsers ? users : nil,
      settings
    ]
    viewControllers = controllers.compactMap { $0 }
  }

}
