//
//  AuthenticationController.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import SwiftyAttributes
import PKHUD

class AuthenticationController: ViewController {

  fileprivate let viewModel = AuthenticationViewModel(with: AppDelegate.shared.providers.authentication)

  fileprivate let appIconImageView = UIImageView.new {
    $0.image = Asset.launchIcon.image
  }

  fileprivate let inputStackView = UIStackView.new {
    $0.axis = .vertical
    $0.alignment = .center
    $0.distribution = .fill
    $0.spacing = 10
  }

  fileprivate let titleLabel = UILabel.new {
    $0.textAlignment = .center
    $0.numberOfLines = 0
  }

  fileprivate let usernameInputView = InputTextField(placeholder: L10n.Authentication.usernamePlaceholder,
                                                     contentType: .emailAddress)

  fileprivate let passwordInputView = InputTextField(placeholder: L10n.Authentication.passwordPlaceholder,
                                                     contentType: .password)

  fileprivate let confirmPasswordInputView = InputTextField(placeholder: L10n.Authentication.confirmPasswordPlaceholder,
                                                            contentType: .password)

  fileprivate let submitButton = UIButton()

  fileprivate let switchModeButton = UIButton()

  override func loadView() {
    super.loadView()

    view.addSubviews(appIconImageView, titleLabel, inputStackView, submitButton, switchModeButton)
    inputStackView.snp.makeConstraints {
      $0.leading.trailing.equalToSuperview().inset(UIEdgeInsets.all(40))
      $0.centerY.equalToSuperview()
    }

    let inputViews = [usernameInputView, passwordInputView, confirmPasswordInputView]
    inputStackView.addArrangedSubviews(inputViews)
    confirmPasswordInputView.isHidden = true

    titleLabel.snp.makeConstraints {
      $0.centerX.equalToSuperview()
      $0.leading.trailing.equalToSuperview().inset(UIEdgeInsets.all(20))
      $0.bottom.equalTo(inputStackView.snp.top).offset(-30)
    }

    submitButton.snp.makeConstraints {
      $0.centerX.equalToSuperview()
      $0.top.equalTo(inputStackView.snp.bottom).offset(30)
    }

    switchModeButton.snp.makeConstraints {
      $0.centerX.equalToSuperview()
      $0.top.equalTo(submitButton.snp.bottom).offset(20)
    }

    appIconImageView.snp.makeConstraints {
      $0.size.equalTo(60)
      $0.centerX.equalToSuperview()
      $0.bottom.equalTo(titleLabel.snp.top).offset(-20)
    }

    for view in inputViews {
      view.delegate = self
    }

  }

  override func viewDidLoad() {
    super.viewDidLoad()

    viewModel.mode.asDriver()
      .drive(onNext: { [weak self] mode in
        guard let `self` = self else {
          return
        }
        switch mode {
        case .login:
          self.updateUIForLoginMode()
        case .register:
          self.updateUIForRegisterMode()
        }
      }).disposed(by: disposeBag)

    switchModeButton.rx.tap.asDriver().drive(viewModel.rx.switchMode).disposed(by: disposeBag)

    submitButton.rx.tap.asDriver().drive(onNext: { [weak self] in
      self?.submitInput()
    }).disposed(by: disposeBag)

    setupKeyboardHandling()
  }

  private func setupKeyboardHandling() {
    NotificationCenter.default.rx
      .notification(Notification.Name.UIKeyboardWillShow, object: nil)
      .subscribe(onNext: { [weak self] notification in
        guard let `self` = self else {
          return
        }
        let height = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height
        self.updateUIForKeyboard(isHidden: false, keyboardHeight: height)
      }).disposed(by: disposeBag)

    NotificationCenter.default.rx
      .notification(Notification.Name.UIKeyboardWillHide, object: nil)
      .subscribe(onNext: { [weak self] _ in
        guard let `self` = self else {
          return
        }
        self.updateUIForKeyboard(isHidden: true, keyboardHeight: nil)
      }).disposed(by: disposeBag)

    let tapGesture = UITapGestureRecognizer()
    view.addGestureRecognizer(tapGesture)
    tapGesture.rx.event.asDriver().drive(onNext: { [weak self] _ in
      self?.view.endEditing(true)
    }).disposed(by: disposeBag)
  }

}

extension AuthenticationController: UITextFieldDelegate {

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return false
  }

}

private extension AuthenticationController {

  var username: String {
    return usernameInputView.text ?? ""
  }

  var password: String {
    return passwordInputView.text ?? ""
  }

  var confirmPassword: String {
    return confirmPasswordInputView.text ?? ""
  }

  func validateInput() -> Bool {
    let username = self.username
    let password = self.password
    let confirmPassword = self.confirmPassword

    guard viewModel.validate(email: username) else {
      showError(with: L10n.Authentication.InputErrors.invalidEmail)
      return false
    }

    let passwordValidity = viewModel.validate(password: password)
    switch passwordValidity {
    case .short:
      showError(with: L10n.Authentication.InputErrors.shortPassword)
      return false
    case .long:
      showError(with: L10n.Authentication.InputErrors.longPassword)
      return false
    default:
      break
    }

    guard viewModel.mode.value == .register else {
      return true
    }

    guard viewModel.validate(password: password, repeatPassword: confirmPassword) else {
      showError(with: L10n.Authentication.InputErrors.invalidRepeatPassword)
      return false
    }

    return true
  }

  func submitInput() {
    guard validateInput() else {
      return
    }
    view.endEditing(true)
    do {
      HUD.show(.progress)
      switch viewModel.mode.value {
      case .login:
        try loginAndLoadUser()
      case .register:
        try registerAndLoadUser()
      }
    } catch {
      showError(with: error.localizedDescription)
      HUD.hide()
    }

  }

  private func loginAndLoadUser() throws {
    let login = try viewModel.login(with: username, password: password)
    authenticateAndLoadUser(for: login)
  }

  private func registerAndLoadUser() throws {
    let registration = try viewModel.register(with: username, password: password)
    authenticateAndLoadUser(for: registration)
  }

  private func authenticateAndLoadUser<E>(for authentication: Observable<AuthenticationModel<E>>) {
    return authentication
      .flatMap { [weak self] _ -> Observable<APISingleModel<UserModel>> in
        guard let `self` = self else {
          return .empty()
        }
        return self.viewModel.loadCurrentUser()
      }
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { _ in
        HUD.hide()
      }, onError: { [weak self] error in
        HUD.hide()
        self?.showError(with: error.localizedDescription)
      }).disposed(by: disposeBag)
  }

}

private extension AuthenticationController {

  func updateTitleLabel(with title: String, subtitle: String) {
    let text = title.withFont(Fonts.Lato.bold.font(size: 22))
      + "\n\(subtitle)".withFont(Fonts.Lato.light.font(size: 18))
    titleLabel.attributedText = text
  }

  func updateSwitchButton(with title: String) {
    let color = Colors.darkText
    let text = title
      .withFont(Fonts.Lato.light.font(size: 18))
      .withTextColor(color)
      .withUnderlineStyle(.styleThick).withUnderlineColor(color)
    switchModeButton.setAttributedTitle(text, for: .normal)
  }

  func updateSubmitButton(with title: String) {
    let text = title
      .withFont(Fonts.Lato.semibold.font(size: 20))
      .withTextColor(Colors.tint)
      .withUnderlineStyle(.styleThick).withUnderlineColor(Colors.tint)
    submitButton.setAttributedTitle(text, for: .normal)
  }

  func updateConfirmPasswordInput(isHidden: Bool) {
    guard confirmPasswordInputView.isHidden != isHidden else {
      return
    }
    UIView.animate(withDuration: 0.3) {
      self.confirmPasswordInputView.isHidden = isHidden
    }
  }

  func updateUIForKeyboard(isHidden: Bool, keyboardHeight: CGFloat?) {
    inputStackView.snp.updateConstraints {
      let offset = (keyboardHeight ?? 0) / 2
      $0.centerY.equalToSuperview().offset(-offset)
    }
    UIView.animate(withDuration: 0.3) {
      self.view.layoutIfNeeded()
    }
  }

}

private extension AuthenticationController {

  func updateUIForLoginMode() {
    updateTitleLabel(with: L10n.Authentication.title, subtitle: L10n.Authentication.subtitleLogin)
    updateSwitchButton(with: L10n.Authentication.noAccountPromotion)
    updateSubmitButton(with: L10n.Authentication.submitLogin)
    updateConfirmPasswordInput(isHidden: true)
  }

}

private extension AuthenticationController {

  private func updateUIForRegisterMode() {
    updateTitleLabel(with: L10n.Authentication.title, subtitle: L10n.Authentication.subtitleRegister)
    updateSwitchButton(with: L10n.Authentication.haveAccountPromotion)
    updateSubmitButton(with: L10n.Authentication.submitRegister)
    updateConfirmPasswordInput(isHidden: false)
  }

}
