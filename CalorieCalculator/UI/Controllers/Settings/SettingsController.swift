//
//  SettingsController.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/9/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import Astrolabe

class SettingsController: TableViewController {

  fileprivate let viewModel = SettingsViewModel()
  private let user: UserModel

  init(with user: UserModel) {
    self.user = user
    super.init()
  }

  required init() {
    fatalError("init() has not been implemented")
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    title = L10n.Settings.title
  }

  override func loadView() {
    super.loadView()
    setupDatasource()
  }

}

private extension SettingsController {

  typealias DailCaloriesCell = TableCell<CaloryThresholdCellView>
  typealias LogoutCell = TableCell<LogoutCellView>

  func setupDatasource() {
    let caloriesCell = DailCaloriesCell(data: viewModel)
    let logoutCell = LogoutCell(data: ()) {
      AppDelegate.shared.providers.authentication.logout()
    }
    let cells: [Cellable?] = [
      user.canHaveMeals ? caloriesCell : nil,
      logoutCell
    ]
    sections = [Section(cells: cells.compactMap { $0 })]
  }

}
