//
//  ShadowedTableCell.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import RxSwift

class ShadowedTableCell: TableViewCell {

  var reuseBag = DisposeBag()
  let safeContentView = ShadowBackgroundView()

  var shadowInsets = UIEdgeInsets.all(5) {
    didSet {
      updateShadowInsets()
    }
  }

  override func setup() {
    super.setup()

    contentView.backgroundColor = Colors.grayBackground
    clipsToBounds = false
    contentView.addSubview(safeContentView)
    safeContentView.snp.makeConstraints {
      $0.edges.equalToSuperview().inset(shadowInsets)
    }
  }

  private func updateShadowInsets() {
    safeContentView.snp.updateConstraints {
      $0.edges.equalToSuperview().inset(shadowInsets)
    }
  }

  override func prepareForReuse() {
    super.prepareForReuse()

    reuseBag = DisposeBag()
  }

}

class ShadowBackgroundView: View {

  override func setup() {
    backgroundColor = .white

    layer.shadowRadius = 2
    layer.shadowOffset = CGSize(width: 1, height: 1)
    layer.shadowOpacity = 0.3
    layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
  }

}
