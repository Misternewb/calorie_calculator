//
//  View.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import RxSwift

class View: UIView {

  let disposeBag = DisposeBag()

  required init() {
    super.init(frame: CGRect.zero)
    setup()
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }

  override class var requiresConstraintBasedLayout: Bool {
    return true
  }

  func setup() {
    translatesAutoresizingMaskIntoConstraints = false
  }

}
