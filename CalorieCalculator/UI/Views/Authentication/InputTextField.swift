//
//  InputTextField.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

class InputTextField: InsettedTextField {

  override var intrinsicContentSize: CGSize {
    var size = super.intrinsicContentSize
    size.width = 220
    return size
  }

  convenience init(placeholder: String, contentType: UITextContentType) {
    self.init()
    font = Fonts.Lato.medium.font(size: 15)
    textColor = Colors.darkText
    textContentType = contentType
    isSecureTextEntry = contentType == .password
    self.placeholder = placeholder
    layer.cornerRadius = 5
    layer.borderWidth = CGFloat.onePixel
    layer.borderColor = Colors.darkText.cgColor
  }

}
