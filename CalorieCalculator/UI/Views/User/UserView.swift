//
//  UserView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import SwiftyAttributes

class UserView: View {

  let nameTextField = InsettedTextField.defaultInstance()
  let passwordTextField = InsettedTextField.defaultInstance()
  let repeatPasswordTextField = InsettedTextField.defaultInstance()
  let calorySelectionView = CalorySelectionView()
  let userRoleView = UserRoleView()

  override func setup() {
    super.setup()

    calorySelectionView.caloryThresholdLabel.font = Fonts.Lato.light.font(size: 14)
    calorySelectionView.caloryThresholdLabel.textAlignment = .center
    let slider = calorySelectionView.slider
    slider.minimumValue = Float(Params.Settings.minCaloryThreshold)
    slider.maximumValue = Float(Params.Settings.maxCaloryThreshold)
    slider.rx.value
      .asDriver(onErrorJustReturn: 0)
      .map { calories -> NSAttributedString in
        let title = L10n.UserEditor.dailyCaloriesTitle.withTextColor(Colors.darkText)
        let caloriesString = String(Int(calories)).withTextColor(Colors.tint)
        return title + caloriesString
      }
      .drive(calorySelectionView.caloryThresholdLabel.rx.attributedText)
      .disposed(by: disposeBag)

    nameTextField.placeholder = L10n.Authentication.usernamePlaceholder
    nameTextField.textContentType = .emailAddress
    passwordTextField.placeholder = L10n.Authentication.passwordPlaceholder
    passwordTextField.textContentType = .password
    passwordTextField.isSecureTextEntry = true
    repeatPasswordTextField.placeholder = L10n.Authentication.confirmPasswordPlaceholder
    repeatPasswordTextField.textContentType = .password
    repeatPasswordTextField.isSecureTextEntry = true

    let subviews = [nameTextField, passwordTextField, repeatPasswordTextField,
                    calorySelectionView, userRoleView]
    let stack = UIStackView(arrangedSubviews: subviews)
    stack.alignment = .center
    stack.distribution = .fill
    stack.axis = .vertical
    stack.spacing = 6
    addSubview(stack)

    subviews.forEach {
      $0.snp.makeConstraints {
        $0.leading.trailing.equalToSuperview()
      }
    }

    stack.snp.makeConstraints {
      $0.edges.equalToSuperview()
    }
  }

}

extension UserView: Reusable {

  typealias Data = UserModel?

  func setup(with data: Data) {
    let isRegistration = data == nil
    nameTextField.isHidden = !isRegistration
    passwordTextField.isHidden = !isRegistration
    repeatPasswordTextField.isHidden = !isRegistration
    let calories = Float(data?.caloriesThreshold ?? Params.Settings.defaultCaloryThreshold)
    calorySelectionView.slider.value = calories
    calorySelectionView.slider.sendActions(for: .valueChanged)
    userRoleView.roles = UserModel.Role.allValues
    userRoleView.currentRole = data?.role ?? .user
  }

  static func size(for data: Data, containerSize: CGSize) -> CGSize {
    return .zero
  }

}
