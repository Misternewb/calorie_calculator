//
//  UserRoleView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

class UserRoleView: View {

  private let segmentedControl = UISegmentedControl()

  var roles: [UserModel.Role] = [] {
    didSet {
      segmentedControl.removeAllSegments()
      for (index, role) in roles.enumerated() {
        segmentedControl.insertSegment(withTitle: role.title, at: index, animated: false)
      }
    }
  }

  var currentRole: UserModel.Role? {
    didSet {
      guard let currentRole = currentRole else {
        return
      }
      segmentedControl.selectedSegmentIndex = roles.index(of: currentRole) ?? 0
    }
  }

  var selectedRole: UserModel.Role {
    let index = segmentedControl.selectedSegmentIndex
    guard roles.count > index, index >= 0 else {
      return .user
    }
    return roles[index]
  }

  override var intrinsicContentSize: CGSize {
    var size = super.intrinsicContentSize
    size.height = 40
    return size
  }

  override func setup() {
    super.setup()
    addSubview(segmentedControl)
    segmentedControl.snp.makeConstraints {
      $0.center.equalToSuperview()
    }
  }

}
