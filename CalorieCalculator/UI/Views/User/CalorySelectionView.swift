//
//  CalorySelectionView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

class CalorySelectionView: View {

  let caloryThresholdLabel = UILabel.new {
    $0.font = Fonts.Lato.medium.font(size: 18)
    $0.numberOfLines = 0
  }

  let slider = UISlider()

  var caloryThreshold: Calory {
    return Calory(slider.value)
  }

  override var intrinsicContentSize: CGSize {
    var size = super.intrinsicContentSize
    size.height = 80
    return size
  }

  override func setup() {
    super.setup()

    addSubviews(caloryThresholdLabel, slider)

    caloryThresholdLabel.snp.makeConstraints {
      $0.leading.trailing.equalToSuperview().inset(UIEdgeInsets.horizontal(15))
      $0.bottom.equalTo(snp.centerY).offset(-15)
    }

    slider.snp.makeConstraints {
      $0.top.equalTo(caloryThresholdLabel.snp.bottom).offset(10)
      $0.leading.trailing.equalTo(caloryThresholdLabel)
    }
  }

}
