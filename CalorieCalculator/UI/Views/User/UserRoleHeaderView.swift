//
//  UserRoleHeaderView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe

class UserRoleHeaderView: TableViewHeaderFooter {

  fileprivate let titleLabel = UILabel.new {
    $0.font = Fonts.Lato.bold.font(size: 18)
    $0.textColor = .white
  }

  override func setup() {
    super.setup()
    contentView.addSubview(titleLabel)
    backgroundView = UIView()
    titleLabel.snp.makeConstraints {
      $0.centerY.equalToSuperview()
      $0.leading.equalTo(20)
    }
  }

}

extension UserRoleHeaderView: Reusable {

  typealias Data = UserModel.Role

  func setup(with data: Data) {
    titleLabel.text = data.title
    backgroundView?.backgroundColor = data.color
  }

  static func size(for data: Data, containerSize: CGSize) -> CGSize {
    return CGSize(width: containerSize.width, height: 40)
  }

}
