//
//  UserCellView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/12/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import RxSwift

class UserCellView: ShadowedTableCell {

  fileprivate let nameLabel = UILabel.new {
    $0.textColor = Colors.darkText
    $0.font = Fonts.Lato.semibold.font(size: 15)
  }

  fileprivate let caloriesLabel = UILabel.new {
    $0.textColor = Colors.darkText
    $0.font = Fonts.Lato.light.font(size: 13)
  }

  fileprivate let roleLabel = UILabel.new {
    $0.font = Fonts.Lato.light.font(size: 13)
  }

  fileprivate let deleteButton = UIButton.new {
    $0.setImage(Asset.Controls.delete.image, for: .normal)
    $0.tintColor = Colors.flatRed
  }

  fileprivate let updateButton = UIButton.new {
    $0.setImage(Asset.Controls.update.image, for: .normal)
    $0.tintColor = Colors.tint
  }

  override func setup() {
    super.setup()

    let labelsStack = UIStackView(arrangedSubviews: [nameLabel, caloriesLabel, roleLabel])
    labelsStack.axis = .vertical
    labelsStack.alignment = .leading
    labelsStack.distribution = .equalSpacing
    labelsStack.spacing = 6

    let actionsStack = UIStackView(arrangedSubviews: [deleteButton, updateButton])
    actionsStack.axis = .vertical
    actionsStack.alignment = .center
    actionsStack.distribution = .equalSpacing
    actionsStack.spacing = 6
    safeContentView.addSubviews(labelsStack, actionsStack)

    labelsStack.snp.makeConstraints {
      $0.leading.equalToSuperview().offset(10)
      $0.centerY.equalToSuperview()
    }

    actionsStack.snp.makeConstraints {
      $0.width.equalTo(60)
      $0.leading.equalTo(labelsStack.snp.trailing)
      $0.trailing.centerY.equalToSuperview()
    }

  }

}

extension UserCellView: Reusable {

  typealias Data = UserCellViewModel

  func setup(with data: Data) {
    let user = data.user
    nameLabel.text = user.email
    caloriesLabel.text = "\(user.caloriesThreshold) \(L10n.User.dailyCaloriesThreshold)"
    roleLabel.attributedText = user.role.title.withTextColor(user.role.color)

    deleteButton.isHidden = !data.deleteEnabled
    updateButton.isHidden = !data.updateEnabled

    deleteButton.rx.tap.subscribe(onNext: {
      data.deleteSubject.onNext(user)
    }).disposed(by: reuseBag)

    updateButton.rx.tap.subscribe(onNext: {
      data.updateSubject.onNext(user)
    }).disposed(by: reuseBag)
  }

  static func size(for data: Data, containerSize: CGSize) -> CGSize {
    return CGSize(width: containerSize.width, height: 100)
  }

}
