//
//  LogoutCellView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/10/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe

class LogoutCellView: ShadowedTableCell {

  fileprivate let titleCell = UILabel.new {
    $0.font = Fonts.Lato.semibold.font(size: 20)
    $0.textColor = Colors.flatRed
    $0.textAlignment = .center
    $0.text = L10n.Settings.logout
  }

  override func setup() {
    super.setup()

    safeContentView.addSubview(titleCell)

    titleCell.snp.makeConstraints {
      $0.center.equalToSuperview()
    }
  }

}

extension LogoutCellView: Reusable {

  typealias Data = Void

  func setup(with data: Data) {
  }

  static func size(for data: Data, containerSize: CGSize) -> CGSize {
    return CGSize(width: containerSize.width, height: 60)
  }

}
