//
//  CaloryThresholdCellView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/10/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import SwiftyAttributes

class CaloryThresholdCellView: ShadowedTableCell {

  fileprivate let calorySelectionView = CalorySelectionView()

  override func setup() {
    super.setup()

    safeContentView.addSubview(calorySelectionView)

    calorySelectionView.snp.makeConstraints {
      $0.leading.trailing.centerY.equalToSuperview()
    }

  }

}

extension CaloryThresholdCellView: Reusable {

  typealias Data = SettingsViewModel

  func setup(with data: Data) {
    calorySelectionView.slider.minimumValue = Float(Params.Settings.minCaloryThreshold)
    calorySelectionView.slider.maximumValue = Float(Params.Settings.maxCaloryThreshold)

    data.dailyCalories.asDriver(onErrorJustReturn: 0)
      .map { Float($0) }
      .drive(calorySelectionView.slider.rx.value)
      .disposed(by: reuseBag)

    calorySelectionView.slider.rx.value.asDriver()
      .map { Int($0) }
      .drive(onNext: { calories in
        data.update(dailyCalories: calories)
      })
      .disposed(by: reuseBag)

    data.dailyCalories.asDriver(onErrorJustReturn: 0)
      .map { calories -> NSAttributedString in
        let title = L10n.Settings.dailyCaloriesTitle.withTextColor(Colors.darkText)
        let caloriesString = String(calories).withTextColor(Colors.tint)
        return title + caloriesString
      }
      .drive(calorySelectionView.caloryThresholdLabel.rx.attributedText)
      .disposed(by: reuseBag)
  }

  static func size(for data: Data, containerSize: CGSize) -> CGSize {
    return CGSize(width: containerSize.width, height: 100)
  }

}
