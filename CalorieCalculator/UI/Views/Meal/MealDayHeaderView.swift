//
//  MealDayHeaderView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/13/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import RxSwift

class MealDayHeaderView: TableViewHeaderFooter {

  fileprivate var reuseBag = DisposeBag()

  fileprivate let dateLabel = UILabel.new {
    $0.font = Fonts.Lato.bold.font(size: 18)
    $0.textColor = .white
  }

  override func setup() {
    super.setup()
    contentView.addSubview(dateLabel)
    backgroundView = UIView()
    dateLabel.snp.makeConstraints {
      $0.centerY.equalToSuperview()
      $0.leading.equalTo(20)
    }
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    reuseBag = DisposeBag()
  }

}

extension MealDayHeaderView: Reusable {

  typealias Data = MealDayViewModel

  func setup(with data: Data) {
    dateLabel.text = "\(data.dateString) - \(data.calories) \(L10n.Meal.calories)"

    guard let backgroundView = backgroundView else {
      return
    }
    data.caloriesBelowThreshold.asDriver(onErrorJustReturn: true)
      .map { $0 ? Colors.flatGreen : Colors.flatRed }
      .drive(backgroundView.rx.backgroundColor).disposed(by: reuseBag)
  }

  static func size(for data: Data, containerSize: CGSize) -> CGSize {
    return CGSize(width: containerSize.width, height: 40)
  }

}
