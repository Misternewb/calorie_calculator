//
//  MealView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe

class MealView: View {

  let nameTextField = InsettedTextField.defaultInstance()

  let caloriesPicker = CaloryPickerView()

  let datePicker = UIDatePicker.new {
    $0.datePickerMode = .dateAndTime
  }

  override func setup() {
    super.setup()

    let stack = UIStackView(arrangedSubviews: [nameTextField, caloriesPicker, datePicker])
    stack.alignment = .center
    stack.distribution = .fill
    stack.axis = .vertical
    addSubview(stack)
    nameTextField.snp.makeConstraints {
      $0.leading.trailing.equalToSuperview()
    }
    caloriesPicker.snp.makeConstraints {
      $0.height.equalTo(90)
    }
    datePicker.snp.makeConstraints {
      $0.height.equalTo(90)
    }
    stack.snp.makeConstraints {
      $0.edges.equalToSuperview()
    }
  }

}

extension MealView: Reusable {

  typealias Data = MealModel?

  func setup(with data: Data) {
    nameTextField.text = data?.name
    caloriesPicker.select(calories: data?.calories ?? Params.Meal.defaultCaloryCount)
    let maxDate = Date()
    datePicker.date = data?.date ?? maxDate
    datePicker.maximumDate = maxDate
  }

  static func size(for data: Data, containerSize: CGSize) -> CGSize {
    return .zero
  }

}
