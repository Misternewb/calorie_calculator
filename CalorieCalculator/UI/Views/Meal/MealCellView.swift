//
//  MealCellView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import RxSwift

class MealCellView: ShadowedTableCell {

  fileprivate let nameLabel = UILabel.new {
    $0.textColor = Colors.darkText
    $0.font = Fonts.Lato.semibold.font(size: 15)
  }

  fileprivate let caloriesLabel = UILabel.new {
    $0.textColor = Colors.darkText
    $0.font = Fonts.Lato.light.font(size: 13)
  }

  fileprivate let dateLabel = UILabel.new {
    $0.textColor = Colors.darkText
    $0.font = Fonts.Lato.light.font(size: 13)
  }

  fileprivate let deleteButton = UIButton.new {
    $0.setImage(Asset.Controls.delete.image, for: .normal)
    $0.tintColor = Colors.flatRed
  }

  fileprivate let updateButton = UIButton.new {
    $0.setImage(Asset.Controls.update.image, for: .normal)
    $0.tintColor = Colors.tint
  }

  override func setup() {
    super.setup()

    let labelsStack = UIStackView(arrangedSubviews: [nameLabel, caloriesLabel, dateLabel])
    labelsStack.axis = .vertical
    labelsStack.alignment = .leading
    labelsStack.distribution = .equalSpacing
    labelsStack.spacing = 6

    let actionsStack = UIStackView(arrangedSubviews: [deleteButton, updateButton])
    actionsStack.axis = .vertical
    actionsStack.alignment = .center
    actionsStack.distribution = .equalSpacing
    actionsStack.spacing = 6
    safeContentView.addSubviews(labelsStack, actionsStack)

    labelsStack.snp.makeConstraints {
      $0.leading.equalToSuperview().offset(10)
      $0.centerY.equalToSuperview()
    }

    actionsStack.snp.makeConstraints {
      $0.width.equalTo(60)
      $0.leading.equalTo(labelsStack.snp.trailing)
      $0.trailing.centerY.equalToSuperview()
    }

  }

}

extension MealCellView: Reusable {

  typealias Data = MealCellViewModel

  func setup(with data: Data) {
    let meal = data.meal
    nameLabel.text = meal.name
    caloriesLabel.text = "\(meal.calories) \(L10n.Meal.calories)"
    dateLabel.text = Formatters.dateTimeFormatter.string(from: meal.date)

    deleteButton.rx.tap.subscribe(onNext: {
      data.deleteSubject.onNext(meal)
    }).disposed(by: reuseBag)

    updateButton.rx.tap.subscribe(onNext: {
      data.updateSubject.onNext(meal)
    }).disposed(by: reuseBag)
  }

  static func size(for data: Data, containerSize: CGSize) -> CGSize {
    return CGSize(width: containerSize.width, height: 100)
  }

}
