//
//  CaloryPickerView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import SwiftyAttributes
import RxSwift

class CaloryPickerView: UIPickerView {

  private let caloryDatasource: [Calory]

  override init(frame: CGRect) {
    let caloriesRange = Params.Meal.minCaloryCount...Params.Meal.maxCaloryCount
    caloryDatasource = Array(caloriesRange).filter { $0 % 5 == 0 }
    super.init(frame: frame)
    delegate = self
    dataSource = self
  }

  convenience init() {
    self.init(frame: .zero)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func select(calories: Calory) {
    guard let index = caloryDatasource.index(of: calories) else {
      return
    }
    selectRow(index, inComponent: 0, animated: false)
  }

  var calories: Calory {
    let row = selectedRow(inComponent: 0)
    return caloryDatasource[row]
  }

}

extension CaloryPickerView: UIPickerViewDataSource {

  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }

  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return caloryDatasource.count
  }

}

extension CaloryPickerView: UIPickerViewDelegate {

  func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int,
                  forComponent component: Int) -> NSAttributedString? {
    let calory = caloryDatasource[row]
    return String(calory).withFont(Fonts.Lato.bold.font(size: 20))
  }

}
