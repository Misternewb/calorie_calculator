//
//  InsettedTextField.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/7/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

class InsettedTextField: UITextField {

  var textInset: CGFloat {
    return 10
  }

  override func textRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: textInset, dy: textInset)
  }

  override func editingRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: textInset, dy: textInset)
  }

  static func defaultInstance() -> InsettedTextField {
    return InsettedTextField.new {
      $0.font = Fonts.Lato.medium.font(size: 18)
      $0.layer.cornerRadius = 5
      $0.layer.borderWidth = CGFloat.onePixel
      $0.layer.borderColor = Colors.tint.cgColor
      $0.textAlignment = .center
    }
  }

}
