//
//  FilterSwitchCellView.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/13/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import Astrolabe
import RxSwift

private enum Constants {

  static let inset: CGFloat = 15

}

class FilterSwitchCellView: ShadowedTableCell {

  let titleLabel = UILabel.new {
    $0.font = Fonts.Lato.medium.font(size: 15)
    $0.textColor = Colors.darkText
    $0.numberOfLines = 0
  }

  let switchView = UISwitch.new {
    $0.onTintColor = Colors.tint
  }

  let datePicker = UIDatePicker.new {
    $0.layer.borderColor = Colors.tint.cgColor
    $0.layer.borderWidth = CGFloat.onePixel
    $0.layer.cornerRadius = 5
    $0.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
  }

  override func setup() {
    super.setup()

    safeContentView.addSubviews(switchView, titleLabel, datePicker)

    setupLayout(isSelected: false)

    titleLabel.snp.makeConstraints {
      $0.centerY.equalTo(switchView)
      $0.leading.equalTo(switchView.snp.trailing).offset(Constants.inset)
      $0.trailing.equalToSuperview().inset(Constants.inset)
    }

  }

  func setupLayout(isSelected: Bool) {
    let inset: CGFloat = Constants.inset
    switchView.snp.remakeConstraints {
      $0.leading.equalToSuperview().offset(inset)
      if isSelected {
        $0.top.equalToSuperview().offset(inset)
      } else {
        $0.centerY.equalToSuperview()
      }
    }

    if isSelected {
      datePicker.snp.remakeConstraints {
        $0.top.equalTo(switchView.snp.bottom).offset(Constants.inset)
        $0.leading.trailing.bottom.equalToSuperview().inset(UIEdgeInsets.all(inset))
      }
    } else {
      datePicker.snp.removeConstraints()
    }
  }

}

extension FilterSwitchCellView: Reusable {

  typealias Data = FilterCellViewModel

  func setup(with data: Data) {
    setupLayout(isSelected: data.selected.value)
    titleLabel.text = data.type.title

    switchView.isOn = data.selected.value
    switchView.rx.isOn.asDriver().skip(1).drive(data.selected).disposed(by: reuseBag)

    datePicker.date = data.date.value
    datePicker.rx.date.asDriver().skip(1).drive(data.date).disposed(by: reuseBag)
    datePicker.datePickerMode = data.type.datePickerMode
    datePicker.isHidden = !data.selected.value
  }

  static func size(for data: Data, containerSize: CGSize) -> CGSize {
    let width = containerSize.width
    let height: CGFloat = data.selected.value ? 200 : 70
    return CGSize(width: width, height: height)
  }

}

private extension FilterCellViewModel.FilterType {

  var datePickerMode: UIDatePickerMode {
    switch self {
    case .fromDate, .toDate: return .date
    case .fromTime, .toTime: return .time
    }
  }

}
