//
//  Newable.swift
//  CalorieCalculator
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import UIKit

public protocol Newable {
  init()
}

extension NSObject: Newable {}

extension Newable where Self: NSObject {

  static func new(_ configuration: ((Self) -> Void)) -> Self {
    let object = self.init()
    configuration(object)
    return object
  }

}

extension Newable where Self: UIView {

  static func new(_ configuration: ((Self) -> Void)) -> Self {
    let view = self.init()
    view.translatesAutoresizingMaskIntoConstraints = false
    configuration(view)
    return view
  }

}
