//
//  FilterTestCase.swift
//  CalorieCalculatorTests
//
//  Created by Sergey Dikovitsky on 2/14/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import XCTest
import Nimble
import RxBlocking

@testable import CalorieCalculator

class FilterTestCase: XCTestCase {

  private let dateFormatter = DateFormatter.new {
    $0.dateFormat = "dd/MM/yyyy HH:mm"
  }

  func testDateFrom() {
    let date = dateFormatter.date(from: "10/01/2018 10:00")!
    let fromDateEarly = dateFormatter.date(from: "01/01/2018 10:00")!
    let fromDateLate = dateFormatter.date(from: "11/01/2018 10:00")!

    var filter = MealFilterModel(fromDate: fromDateEarly, toDate: nil, fromTime: nil, toTime: nil)
    expect(filter.apply(to: date)).to(beTrue())
    filter.fromDate = fromDateLate
    expect(filter.apply(to: date)).to(beFalse())
    filter.fromDate = date
    expect(filter.apply(to: date)).to(beTrue())
  }

  func testDateTo() {
    let date = dateFormatter.date(from: "10/01/2018 10:00")!
    let toDateEarly = dateFormatter.date(from: "01/01/2018 10:00")!
    let toDateLate = dateFormatter.date(from: "11/01/2018 10:00")!

    var filter = MealFilterModel(fromDate: nil, toDate: toDateEarly, fromTime: nil, toTime: nil)
    expect(filter.apply(to: date)).to(beFalse())
    filter.toDate = toDateLate
    expect(filter.apply(to: date)).to(beTrue())
    filter.toDate = date
    expect(filter.apply(to: date)).to(beTrue())
  }

  func testTimeFrom() {
    let date = dateFormatter.date(from: "10/01/2018 10:00")!
    let fromTimeEarly = dateFormatter.date(from: "01/01/2018 09:00")!
    let fromTimeLate = dateFormatter.date(from: "11/01/2018 11:00")!

    var filter = MealFilterModel(fromDate: nil, toDate: nil, fromTime: fromTimeEarly, toTime: nil)
    expect(filter.apply(to: date)).to(beTrue())
    filter.fromTime = fromTimeLate
    expect(filter.apply(to: date)).to(beFalse())
    filter.fromTime = date
    expect(filter.apply(to: date)).to(beTrue())
  }

  func testTimeTo() {
    let date = dateFormatter.date(from: "10/01/2018 10:00")!
    let toTimeEarly = dateFormatter.date(from: "01/01/2018 09:00")!
    let toTimeLate = dateFormatter.date(from: "11/01/2018 11:00")!

    var filter = MealFilterModel(fromDate: nil, toDate: nil, fromTime: nil, toTime: toTimeEarly)
    expect(filter.apply(to: date)).to(beFalse())
    filter.toTime = toTimeLate
    expect(filter.apply(to: date)).to(beTrue())
    filter.toTime = date
    expect(filter.apply(to: date)).to(beTrue())
  }

  func testCombined() {
    let date1 = dateFormatter.date(from: "10/01/2018 10:00")!
    let date2 = dateFormatter.date(from: "10/02/2018 10:00")!
    let date3 = dateFormatter.date(from: "10/02/2018 18:00")!

    var filter = MealFilterModel(fromDate: dateFormatter.date(from: "10/02/2018 10:00")!,
                                 toDate: dateFormatter.date(from: "10/03/2018 10:00")!,
                                 fromTime: nil, toTime: nil)
    expect(filter.apply(to: date1)).to(beFalse())
    expect(filter.apply(to: date2)).to(beTrue())
    expect(filter.apply(to: date3)).to(beTrue())

    filter.fromTime = dateFormatter.date(from: "10/01/2018 14:00")!

    expect(filter.apply(to: date1)).to(beFalse())
    expect(filter.apply(to: date2)).to(beFalse())
    expect(filter.apply(to: date3)).to(beTrue())

    filter.toTime = dateFormatter.date(from: "10/01/2018 17:00")!

    expect(filter.apply(to: date1)).to(beFalse())
    expect(filter.apply(to: date2)).to(beFalse())
    expect(filter.apply(to: date3)).to(beFalse())

    filter.fromDate = nil
    filter.toDate = nil
    filter.fromTime = dateFormatter.date(from: "10/01/2018 09:00")!

    expect(filter.apply(to: date1)).to(beTrue())
    expect(filter.apply(to: date2)).to(beTrue())
    expect(filter.apply(to: date3)).to(beFalse())
  }

}
