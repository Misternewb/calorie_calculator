//
//  AuthenticationViewModelTestCase.swift
//  CalorieCalculatorTests
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import XCTest
import Nimble
import RxBlocking

@testable import CalorieCalculator

class AuthenticationViewModelTestCase: XCTestCase {

  let viewModel = AuthenticationViewModel(with: AuthenticationManager(with: StorageManager()))

  func testValidEmail() {
    expect(self.viewModel.validate(email: "dikovitsky.s@gmail.com")).to(beTrue())
  }

  func testInvalidEmail() {
    expect(self.viewModel.validate(email: "dikovitsky.s@@gmail.com")).to(beFalse())
  }

  func testValidPassword() {
    expect(self.viewModel.validate(password: "syndykzamok"))
      .to(equal(AuthenticationViewModel.PasswordValidity.valid))
  }

  func testShortPassword() {
    expect(self.viewModel.validate(password: "test"))
      .to(equal(AuthenticationViewModel.PasswordValidity.short))
  }

  func testLongPassword() {
    let password = "bigpasswordbigpasswordbigpasswordbigpasswordbigpasswordbigpasswordbigpassword"
    expect(self.viewModel.validate(password: password))
      .to(equal(AuthenticationViewModel.PasswordValidity.long))
  }

  func testValidRepeatPasswords() {
    expect(self.viewModel.validate(password: "validrepeat", repeatPassword: "validrepeat")).to(beTrue())
  }

  func testInvalidRepeatPasswords() {
    expect(self.viewModel.validate(password: "password", repeatPassword: "anotherpassword")).to(beFalse())
  }

}
