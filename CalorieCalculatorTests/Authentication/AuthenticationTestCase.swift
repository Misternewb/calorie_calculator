//
//  AuthenticationTestCase.swift
//  CalorieCalculatorTests
//
//  Created by Sergey Dikovitsky on 2/5/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import XCTest
import Nimble
import RxBlocking

@testable import CalorieCalculator

class AuthenticationTestCase: XCTestCase {

  private let validEmail = "dikovitsky.s@gmail.com"
  private let validPassword = "syndykzamok"

  let authenticationManager = AuthenticationManager(with: StorageManager())

  override func setUp() {
    super.setUp()
    Nimble.AsyncDefaults.Timeout = 7
    URLCache.shared.removeAllCachedResponses()
  }

  override func tearDown() {
    super.tearDown()
    authenticationManager.logout()
  }

  func testValidLogin() {
    let email = validEmail
    do {
      let response = try authenticationManager.login(with: email,
                                                     password: validPassword)
      guard let result = try response.toBlocking().first() else {
        throw "Can not extract response"
      }
      expect(result.auth.email).to(equal(email))
      expect(self.authenticationManager.auth.value).notTo(be(nil))
    } catch {
      fail(error.localizedDescription)
    }
  }

  func testInvalidEmailLogin() {
    let email = "dikovitsky.ss@gmail.com"
    do {
      let response = try authenticationManager.login(with: email,
                                                     password: "syndykzamok")
      _ = try response.toBlocking().first()
      fail("User with such email does not exist")
    } catch let error as LoginError {
      expect(error).to(equal(LoginError.emailNotFound))
    } catch {
      fail(error.localizedDescription)
    }
  }

  func testInvalidPasswordLogin() {
    let email = "dikovitsky.s@gmail.com"
    do {
      let response = try authenticationManager.login(with: email,
                                                     password: "syndykzamok1")
      _ = try response.toBlocking().first()
      fail("Invalid password enter")
    } catch let error as LoginError {
      expect(error).to(equal(LoginError.invalidPassword))
    } catch {
      fail(error.localizedDescription)
    }
  }

  func testRegistration() {
    let email = "test@gmail.com"
    do {
      let response = try authenticationManager.register(with: email,
                                                        password: "password")
      guard let auth = try response.toBlocking().first()?.auth else {
        throw "Can not extract response"
      }
      expect(auth.email).to(equal(email))
      expect(self.authenticationManager.auth.value).notTo(be(nil))
    } catch let error as RegisterError {
      expect(error).to(equal(RegisterError.emailExists))
    } catch {
      fail(error.localizedDescription)
    }
  }

  func testLoginAndLoadUser() {
    let email = validEmail
    do {
      let response = try authenticationManager.login(with: email,
                                                     password: validPassword)
      guard let authResult = try response.toBlocking().first() else {
        throw "Can not extract auth"
      }

      expect(self.authenticationManager.auth.value).notTo(beNil())
      expect(self.authenticationManager.tokenProvider).notTo(beNil())
      guard let userResult = try authenticationManager.loadCurrentUser().toBlocking().first() else {
        throw "Can not extract user"
      }

      expect(authResult.auth.id).to(equal(userResult.model.id))
      expect(self.authenticationManager.user).notTo(beNil())
    } catch {
      fail(error.localizedDescription)
    }
  }

}
