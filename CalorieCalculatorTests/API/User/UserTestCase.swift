//
//  UserTestCase.swift
//  CalorieCalculatorTests
//
//  Created by Sergey Dikovitsky on 2/11/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import XCTest
import Nimble
import RxBlocking

@testable import CalorieCalculator

class UserTestCase: APITestCase {

  private static let userProvider: UserProvider = UserManager(with: authenticationProvider)

  func testGetUsers() {
    do {
      guard let userResult = try UserTestCase.userProvider.getUsers(cached: false).toBlocking().first() else {
        throw "Can not load users"
      }
      expect(userResult.models.isEmpty).to(beFalse())
    } catch {
      fail(error.localizedDescription)
    }
  }

  func testGetUser() {
    do {
      guard let currentUser = UserTestCase.authenticationProvider.user.value else {
        throw "No current user"
      }
      guard let user = try UserTestCase.userProvider.getUser().toBlocking().first() else {
        throw "Can not load user"
      }
      expect(currentUser).to(equal(user.model))
    } catch {
      fail(error.localizedDescription)
    }
  }

  func testCRUDUsers() {
    do {
      let userProvider = UserTestCase.userProvider
      let userEmail = "testcrudser@gmail.com"
      _ = try userProvider.createUser(username: userEmail, password: "testest").toBlocking().first()

      guard let users = try userProvider.getUsers(cached: false).toBlocking().first() else {
        throw "Can not load users"
      }
      guard var createdUser = users.models.first(where: { $0.email == userEmail }) else {
        throw "User was not created"
      }
      createdUser.caloriesThreshold = Params.Settings.maxCaloryThreshold
      _ = try userProvider.update(user: createdUser).toBlocking().first()

      guard let updatedUsers = try userProvider.getUsers(cached: false).toBlocking().first() else {
          throw "Can not load users"
      }
      let hasUpdatedUser = updatedUsers.models.contains(createdUser)
      expect(hasUpdatedUser).to(beTrue())

      _ = try userProvider.delete(user: createdUser).toBlocking().first()
      guard let finalUsers = try userProvider.getUsers(cached: false).toBlocking().first() else {
        throw "Can not load users"
      }
      let contains = finalUsers.models.contains(createdUser)
      expect(contains).to(beFalse())
      expect(users.models.count).to(be(finalUsers.models.count + 1))
    } catch {
      fail(error.localizedDescription)
    }
  }

}
