//
//  APITestCase.swift
//  CalorieCalculatorTests
//
//  Created by Sergey Dikovitsky on 2/11/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import XCTest
import Nimble
import RxBlocking

@testable import CalorieCalculator

class APITestCase: XCTestCase {

  static let authenticationProvider = AuthenticationManager(with: StorageManager())

  override class func setUp() {
    super.setUp()

    do {
      _ = try authenticationProvider.login(with: "dikovitsky.s@gmail.com", password: "syndykzamok").toBlocking().first()
      _ = try authenticationProvider.loadCurrentUser().toBlocking().first()
    } catch {
      fail(error.localizedDescription)
    }
  }

  override class func tearDown() {
    super.tearDown()
    authenticationProvider.logout()
  }

}
