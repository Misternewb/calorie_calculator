//
//  MealTestCase.swift
//  CalorieCalculatorTests
//
//  Created by Sergey Dikovitsky on 2/6/18.
//  Copyright © 2018 Sergey Dikovitsky. All rights reserved.
//

import XCTest
import Nimble
import RxBlocking

@testable import CalorieCalculator

class MealTestCase: APITestCase {

  private static let mealProvider: MealProvider = MealManager(with: authenticationProvider)

  func testGetMeals() {
    do {
      guard let user = MealTestCase.authenticationProvider.user.value else {
        throw "No user"
      }
      guard let mealResult = try MealTestCase.mealProvider.getMeal(for: user, cached: false).toBlocking().first() else {
        throw "Can not load meals"
      }
      expect(mealResult.models.isEmpty).to(beFalse())
    } catch {
      fail(error.localizedDescription)
    }
  }

  func testCRUDMeals() {
    do {
      guard let user = MealTestCase.authenticationProvider.user.value else {
        throw "No user"
      }
      let mealProvider = MealTestCase.mealProvider
      let calories = Int.random(Params.Meal.minCaloryCount...Params.Meal.maxCaloryCount)
      let meal = MealModel(with: nil, name: "Meat", date: Date(), calories: calories)
      _ = try mealProvider.create(meal: meal, user: user).toBlocking().first()

      guard let meals = try mealProvider.getMeal(for: user, cached: false).toBlocking().first() else {
        throw "Can not load meals"
      }
      guard let createdMeal = meals.models.first(where: {
        $0.name == meal.name && $0.calories == meal.calories
      }) else {
        throw "Can not find created meal"
      }

      let updateMeal = MealModel(with: createdMeal.id, name: createdMeal.name,
                                 date: createdMeal.date, calories: 350)
      _ = try mealProvider.update(meal: updateMeal, user: user).toBlocking().first()

      guard let updatedMeals = try mealProvider.getMeal(for: user, cached: false).toBlocking().first() else {
        throw "Can not load meals"
      }
      guard let updatedMeal = updatedMeals.models.first(where: { $0.id == updateMeal.id }) else {
        throw "Can not find updated meal"
      }
      expect(updatedMeal.calories).to(be(updateMeal.calories))

      _ = try mealProvider.delete(meal: updatedMeal, user: user).toBlocking().first()
      guard let finalMeals = try mealProvider.getMeal(for: user, cached: false).toBlocking().first() else {
        throw "Can not load meals"
      }
      let contains = finalMeals.models.contains(where: { $0.id == updatedMeal.id })
      expect(contains).to(beFalse())
      expect(meals.models.count).to(be(finalMeals.models.count + 1))
    } catch {
      fail(error.localizedDescription)
    }
  }

}
