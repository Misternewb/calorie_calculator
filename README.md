# Cal Cal
Cal Cal is an iOS aplication, which features:

  - unique way to calculate your calories
  - clean and pleasing UI
  - great user and meal administration
### Tech information
  - iOS 11+
  - Swift 4.1
  - Xcode 9.3+
  - [Code style](https://github.com/raywenderlich/swift-style-guide)
  - For better readabilty, it is advised to use 2 spaces length tabs/indents
### Installation

Clone or download source code.
Install CocoaPods dependencies.
```sh
$ cd path/to/project
$ pod install
```

